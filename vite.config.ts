import react from "@vitejs/plugin-react";
import { defineConfig } from "vite";
import svgrPlugin from "vite-plugin-svgr";

// https://vitejs.dev/config/
export default defineConfig({
    plugins: [
        svgrPlugin({
            svgrOptions: {
                icon: false,
                // ...svgr options (https://react-svgr.com/docs/options/)
            },
        }),
        react(),
    ],
    server: {
        fs: {
            // Allow serving files from one level up to the project root
            allow: [".."],
        },
    },
});
