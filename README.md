# Irrigation Web Application

## Features

-   Kunbernets deployment file
-   Docker file
-   Plot Land Management
-   React with ReactQuery hook and ReactForm hook
-   Chakra UI components

### Install npm modules

`npm i`

### Run web app

`npm start`

## Screenshots

![alt text](screenshots/view.png)

![alt text](screenshots/create.png)
