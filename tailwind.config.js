module.exports = {
    content: ["./index.html", "./src/**/*.{vue,js,ts,jsx,tsx}"],
    darkMode: "media", // or 'media' or 'class'
    theme: {
        extend: {
            fontSize: {
                xss: ".625rem",
            },
            colors: {
                yellow: {
                    950: "#FCA549",
                },
                blue: {
                    550: "#4E73F8",
                    551: "rgba(78, 115, 248, 0.08)",
                },
            },
            width: {
                9.5: "2.375rem",
            },
            height: {
                9.5: "2.375rem",
            },
        },
        minHeight: {
            16: "4rem",
        },
    },
    variants: {
        extend: {
            cursor: ["disabled"],
            opacity: ["disabled"],
        },
    },
    plugins: [
        require("@tailwindcss/forms"),
        require("@tailwindcss/typography"),
        require("@tailwindcss/aspect-ratio"),
        require("tailwindcss-rtl"),
    ],
};
