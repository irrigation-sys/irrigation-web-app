import { createContext, useContext } from 'react';

export const dashboardContext = createContext<any>(null);

export function useDashboardState() {
    return useContext(dashboardContext);
}
