import { Route } from "react-router-dom";
import PageBase from "./screens/PageBase";

export function PrivateRoute({ children, ...rest }: any) {
    return <Route {...rest} render={({ location }) => <PageBase>{children}</PageBase>} />;
}
