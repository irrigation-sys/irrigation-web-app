import { BrowserRouter, Switch } from "react-router-dom";
import { PrivateRoute } from "./PrivateRoute";
import Home from "./screens/Home";
import CreatePlotLand from "./screens/plot-land/CreatePlotLand";
import EditPlotLand from "./screens/plot-land/EditPlotLand";
import PlotLands from "./screens/plot-land/PlotLands";

const MainRoutes = () => (
    <main>
        <BrowserRouter>
            <Switch>
                <PrivateRoute exact path="/" render={() => <Home />} />
                <PrivateRoute exact path="/home" render={() => <Home />} />
                <PrivateRoute path="/PlotLands">
                    <PlotLands />
                </PrivateRoute>
                <PrivateRoute path="/create-PlotLand">
                    <CreatePlotLand />
                </PrivateRoute>
                <PrivateRoute path="/edit-PlotLand">
                    <EditPlotLand />
                </PrivateRoute>
            </Switch>
        </BrowserRouter>
    </main>
);

export default MainRoutes;
