import { Box, Heading, Spinner } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

export function loadingIndicator() {
    const { t } = useTranslation();

    return (
        <Box w="100%" h="50vh" display={"flex"} flexDirection="column" justifyContent={"center"} alignItems="center">
            <Heading fontSize={"18px"} fontWeight="600" marginY={6}>
                {t("Please wait...")}
            </Heading>
            <Box>
                <Spinner />
            </Box>
        </Box>
    );
}
