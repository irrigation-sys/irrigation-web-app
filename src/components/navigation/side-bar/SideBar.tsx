import AdminSideBar from "./AdminSideBar";
import "./SideBar.css";

export default function Sidebar() {
    return <AdminSideBar />;
}
