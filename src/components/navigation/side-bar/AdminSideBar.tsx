import {
    Accordion,
    AccordionButton,
    AccordionItem,
    AccordionPanel,
    Box,
    chakra,
    Flex,
    HStack,
    Text,
    VStack,
} from "@chakra-ui/react";
import { useState } from "react";
import { useTranslation } from "react-i18next";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { useHistory } from "react-router-dom";
import { ReactComponent as BrandLogo } from "../../../assets/brandLogo.svg";
import { ReactComponent as CourtHouseIcon } from "../../../assets/navigation/courthouse.svg";
import { ReactComponent as CourtHouseBlueIcon } from "../../../assets/navigation/courthouseBlue.svg";
import { useDashboardState } from "../../../context/DashboardContext";
import "./SideBar.css";

export default function AdminSideBar() {
    const [navSize, changeNavSize] = useState("large");
    const history = useHistory();
    const {
        isSideBarOpen,
        setIsSideBarOpen,
        selectedPageIndex,
        setSelectedPageIndex,
        setSelectedSubPageIndex,
        selectedSubPageIndex,
    } = useDashboardState();
    const {
        t,
        i18n: { language },
    } = useTranslation();

    const blueRect = () => <Box marginInlineEnd={"10px"} className="blue-rect"></Box>;

    const invisbleRect = () => <Box marginInlineEnd={"10px"} className="invis-rect"></Box>;

    const isSelected = (index: number) => selectedPageIndex === index;

    const isSubPageSelected = (index: number) => selectedSubPageIndex === index;

    const headingText = (index: number, title: string) => (
        <chakra.span
            alignSelf={"center"}
            fontSize={"14px"}
            fontWeight={"600"}
            color={isSelected(index) ? "#4E73F8" : "black"}>
            {t(`${title}`)}
        </chakra.span>
    );

    function subPage(pathUrl: string, header: string, index: number) {
        return (
            <>
                <HStack
                    width={"80%"}
                    marginEnd={"16px"}
                    marginStart={"16px"}
                    marginTop={"10px"}
                    cursor="pointer"
                    onClick={() => {
                        history.push(pathUrl);
                        setSelectedSubPageIndex(index);
                    }}>
                    {language === "en" ? (
                        <IoIosArrowForward color={isSubPageSelected(index) ? "#4E73F8" : "black"} />
                    ) : (
                        <IoIosArrowBack color={isSubPageSelected(index) ? "#4E73F8" : "black"} />
                    )}
                    <Text fontSize={"12px"} fontWeight={"600"} color={isSubPageSelected(index) ? "#4E73F8" : "black"}>
                        {header}
                    </Text>
                </HStack>
            </>
        );
    }

    return (
        <Box display={isSideBarOpen ? "block" : "none"}>
            <Flex
                left="5"
                boxShadow="0 4px 12px 0 rgba(0, 0, 0, 0.05)"
                w={navSize == "small" ? "75px" : "264px"}
                flexDir="column"
                backgroundColor="brandLightGrey"
                margin={"0"}>
                <Flex
                    p="5%"
                    flexDir="column"
                    h="100%"
                    alignItems={navSize == "small" ? "center" : "flex-start"}
                    mb={4}
                    marginInlineEnd={"4px"}
                    padding="0">
                    <Box mt={4} height="92vh" textAlign="center" alignContent="center" width="100%">
                        <Flex marginBottom={"40px"} textAlign={"center"} width="100%" justifyContent={"space-between"}>
                            <Box marginInlineStart={"24px"}>
                                <BrandLogo />
                            </Box>
                            {language === "en" ? (
                                <IoIosArrowBack
                                    onClick={() => {
                                        setIsSideBarOpen(false);
                                    }}
                                    className="alignMid"
                                    cursor="pointer"
                                />
                            ) : (
                                <IoIosArrowForward
                                    onClick={() => {
                                        setIsSideBarOpen(false);
                                    }}
                                    className="alignMid"
                                    cursor="pointer"
                                />
                            )}
                        </Flex>
                        <VStack>
                            <Accordion allowToggle width={"100%"} margin={"0"}>
                                <AccordionItem
                                    width={"100%"}
                                    margin={"0"}
                                    padding={"0"}
                                    onClick={() => setSelectedPageIndex(0)}>
                                    <AccordionButton>
                                        <Box flex="1" textAlign="left" margin={"0"}>
                                            <HStack>
                                                {isSelected(0) ? blueRect() : invisbleRect()}
                                                <HStack>
                                                    {isSelected(0) ? <CourtHouseBlueIcon /> : <CourtHouseIcon />}
                                                    <Text alignSelf={"center"} fontSize={"14px"} fontWeight={"600"}>
                                                        {headingText(0, "Plot Lands Management")}
                                                    </Text>
                                                </HStack>
                                            </HStack>
                                        </Box>
                                    </AccordionButton>
                                    <AccordionPanel pb={4}>
                                        {subPage("/PlotLands", t("Plot lands Management"), 0.0)}
                                    </AccordionPanel>
                                </AccordionItem>
                            </Accordion>
                        </VStack>
                    </Box>
                </Flex>
            </Flex>
        </Box>
    );
}
