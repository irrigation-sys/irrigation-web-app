import { Input, InputGroup, InputLeftElement, Select, Stack } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { FaSearch } from "react-icons/fa";

export default function App() {
    const { t } = useTranslation();
    return (
        <Stack spacing={4} size={"lg"} className="customIcon">
            <InputGroup size={"lg"} borderRadius={"md"} backgroundColor="brandLightGrey" maxWidth="100%">
                <InputLeftElement border="none" pointerEvents="none" children={<FaSearch fontSize={"16px"} />} />
                <Input
                    border="none"
                    outline="none"
                    width="300px"
                    type="text"
                    fontSize="14px"
                    fontWeight={"500"}
                    placeholder={t("Search")}
                />
                <Select fontSize={"14px"} placeholder={t("Plot Lands")} border="none" width="130px" marginRight="5px">
                    <option value="option1">{t("Sensors")}</option>
                </Select>
            </InputGroup>
        </Stack>
    );
}
