import { HStack } from "@chakra-ui/react";
import { useHistory } from "react-router-dom";
import NotificationMenu from "./NotificationMenu";

export default function NotificationPanel() {
    const history = useHistory();

    return (
        <>
            <HStack spacing={"8px"}>
                <NotificationMenu />
            </HStack>
        </>
    );
}
