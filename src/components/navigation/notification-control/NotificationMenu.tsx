import {
    Avatar,
    AvatarBadge,
    Box,
    Button,
    Center,
    Divider,
    HStack,
    IconButton,
    Image,
    Menu,
    MenuButton,
    MenuItem,
    MenuList,
    Text,
    VStack,
} from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { BiDotsVerticalRounded } from "react-icons/bi";
import { FaCheck } from "react-icons/fa";
import { IoLogoWechat } from "react-icons/io5";
import { useHistory } from "react-router-dom";
import notifyGreenIcon from "../../../assets/navigation/notify.png";
import { ReactComponent as NotificationCircleIcon } from "../../../assets/notification-circle-vector.svg";
import { NOTIFICATION_TYPES } from "../../../models/enums/enums";
import ScrollableContainer from "../../ScrollableContainer";
import { transformDate } from "../../Utils";

export default function NotificationMenu() {
    const {
        t,
        i18n: { language },
    } = useTranslation();

    const history = useHistory();
    const notificationList = [
        {
            fromName: "Youmi",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "POST",
        },
        {
            fromName: "Shery",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "POST",
        },
        {
            fromName: "Momo",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "COMMENT",
        },
        {
            fromName: "Tina",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "COMMENT",
        },
        {
            fromName: "Lana",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "COMMENT",
        },
        {
            fromName: "Lara",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "POST",
        },
        {
            fromName: "Mayan",
            message:
                "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et",
            sendDate: 1642458218131,
            type: "COMMENT",
        },
    ];
    const renderNotificationBadge = (notificationType: string) => {
        switch (notificationType) {
            case NOTIFICATION_TYPES.POST:
                return (
                    <Avatar width={"40px"} height={"40px"}>
                        <AvatarBadge width={"20px"} height={"20px"} borderColor={"#FCA549"} bg="#FCA549">
                            <NotificationCircleIcon width={"12px"} height={"12px"} color="white" />
                        </AvatarBadge>
                    </Avatar>
                );
            case NOTIFICATION_TYPES.COMMENT:
                return (
                    <Avatar width={"40px"} height={"40px"}>
                        <AvatarBadge width={"20px"} height={"20px"} borderColor={"#4E73F8"} bg={"#4E73F8"}>
                            <IoLogoWechat width={"12px"} height={"12px"} color="white" />
                        </AvatarBadge>
                    </Avatar>
                );
            default:
                return "";
        }
    };
    const renderActionMenu = () => {
        return (
            <Menu>
                <MenuButton
                    mt={"30%"}
                    pr={language == "en" ? "24px" : ""}
                    pl={language == "ar" ? "24px" : ""}
                    border={"none"}
                    as={IconButton}
                    borderRadius="50%"
                    aria-label="Options"
                    icon={<BiDotsVerticalRounded />}
                    variant="outline"
                    width={"9px"}
                    height={"16px"}
                    _hover={{ background: "none" }}
                    _focus={{ outline: "none" }}
                    _active={{ background: "none" }}
                />
                <MenuList fontSize={"12px"}>
                    <MenuItem icon={<FaCheck />}>{t("Mark As Read")}</MenuItem>
                </MenuList>
            </Menu>
        );
    };
    return (
        <div>
            <Menu>
                <MenuButton
                    size={"lg"}
                    backgroundColor="brandLightGrey"
                    aria-label="notifications"
                    as={IconButton}
                    variant="outline"
                    icon={<Image width={"28px"} height={"24px"} src={notifyGreenIcon} />}></MenuButton>
                <MenuList>
                    <ScrollableContainer width={"372px"} height={"473px"} maxHeight={"300px"}>
                        <div className="grid grid-cols-2 m-4 gap-x-2">
                            <span className="pt-2 font-semibold">{t("Notifications")}</span>
                            <Button
                                fontSize={"10px"}
                                variant="ghost"
                                size={"sm"}
                                className="float-right ml-3"
                                textColor="gray.600"
                                leftIcon={<FaCheck />}>
                                {t("Mark All As Read")}
                            </Button>
                        </div>
                        {notificationList?.map((notification, index) => (
                            <div key={`${index}0`}>
                                <HStack alignItems="start" key={index}>
                                    <Box key={`${index}1`}>
                                        <MenuItem>
                                            <VStack alignItems="start" className="">
                                                <HStack>
                                                    {renderNotificationBadge(notification.type)}
                                                    <Box>
                                                        <Text
                                                            w={"230px"}
                                                            h={"32px"}
                                                            style={{
                                                                whiteSpace: "normal",
                                                                wordWrap: "break-word",
                                                            }}
                                                            fontSize={"12px"}
                                                            isTruncated
                                                            className="break-all">
                                                            <span className="font-semibold">
                                                                {notification.fromName}
                                                            </span>
                                                            {notification.message}
                                                        </Text>
                                                    </Box>
                                                </HStack>
                                                <Text
                                                    position={"relative"}
                                                    w={"300px"}
                                                    h={"18px"}
                                                    left={language == "en" ? "56px" : ""}
                                                    right={language == "ar" ? "56px" : ""}
                                                    top={"1.5px"}
                                                    fontSize={"10px"}
                                                    textColor="gray.400">
                                                    {transformDate(new Date(notification.sendDate), language)}
                                                </Text>
                                            </VStack>
                                        </MenuItem>
                                    </Box>
                                    <Box key={`${index}2`}>{renderActionMenu()}</Box>
                                </HStack>
                                <Center key={`${index}3`}>
                                    <Divider key={`${index}4`} w={"300px"} />
                                </Center>
                            </div>
                        ))}
                        <Center pt={"5px"}>
                            <Button
                                width={"141px"}
                                height={"24px"}
                                style={{ backgroundColor: "rgba(78, 115, 248, 0.1)" }}
                                borderRadius={"44px"}
                                fontSize={"10px"}
                                variant="ghost"
                                textColor={"#4E73F8"}
                                _hover={{ bg: "#4E73F8" }}
                                _focus={{ outline: "none" }}
                                onClick={() => history.push("/notifications")}>
                                {t("Show More Notifications")}
                            </Button>
                        </Center>
                    </ScrollableContainer>
                </MenuList>
            </Menu>
        </div>
    );
}
