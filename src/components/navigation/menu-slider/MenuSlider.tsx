import { CSSProperties, ReactChild, ReactChildren, useState } from "react";
import { Carousel } from "react-responsive-carousel";
import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./slider.css";
import { Box } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

export default function MenuSlider({
    children,
    previousButton,
    nextButton,
}: {
    children: ReactChild[];
    previousButton: ReactChild;
    nextButton: ReactChild;
}) {
    const arrowStyles: CSSProperties = {
        zIndex: 2,
        cursor: "pointer",
    };

    const {
        i18n: { dir },
    } = useTranslation();

    const [selected, setSelected] = useState(0);

    const goPrevious = () => setSelected(selected - 1);

    const goNext = () => setSelected(selected + 1);

    return (
        <Box
            style={{
                direction: "initial",
            }}>
            <Carousel
                selectedItem={selected}
                showThumbs={false}
                showIndicators={false}
                showStatus={false}
                renderArrowPrev={() =>
                    ((dir() === "rtl" && selected < children.length - 1) || (dir() === "ltr" && selected > 0)) && (
                        <Box
                            label="label"
                            onClick={() => (dir() === "rtl" ? goNext() : goPrevious())}
                            style={{ direction: dir(), ...arrowStyles }}>
                            {previousButton}
                        </Box>
                    )
                }
                renderArrowNext={() =>
                    ((dir() === "rtl" && selected > 0) || (dir() === "ltr" && selected < children.length - 1)) && (
                        <Box
                            label="label"
                            onClick={() => (dir() === "rtl" ? goPrevious() : goNext())}
                            style={{ direction: dir(), ...arrowStyles }}>
                            {nextButton}
                        </Box>
                    )
                }>
                {children.map((e, index) => {
                    return (
                        <Box
                            key={index}
                            style={{
                                direction: dir(),
                            }}>
                            {e}
                        </Box>
                    );
                })}
            </Carousel>
        </Box>
    );
}
