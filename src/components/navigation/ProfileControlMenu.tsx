import { Avatar, Button, Menu, MenuButton, MenuItem, MenuList } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";
import { BiLock, BiUserCircle } from "react-icons/bi";
import { FaChevronDown } from "react-icons/fa";
import { GrUserSettings } from "react-icons/gr";
import { RiUserReceived2Line } from "react-icons/ri";

export function ProfileControlMenu() {
    const {
        t,
        i18n: { language },
    } = useTranslation();
    return (
        <>
            <Menu>
                <MenuButton
                    size={"lg"}
                    backgroundColor="brandLightGrey"
                    as={Button}
                    fontSize={"14px"}
                    rightIcon={<FaChevronDown fontSize={"10.5px"} />}
                    paddingLeft="10px"
                    leftIcon={<Avatar width={"24px"} height={"24px"} alignSelf="center" margin="auto" />}>
                    {"Mawaziny"}
                </MenuButton>
                <MenuList>
                    <MenuItem iconSpacing={"5px"} icon={<GrUserSettings size={"20px"} />}>
                        <span>{t("Edit Profile")}</span>
                    </MenuItem>
                    <MenuItem iconSpacing={"5px"} icon={<BiUserCircle size={"20px"} />}>
                        <span>{t("User Manual")}</span>
                    </MenuItem>
                    <MenuItem iconSpacing={"5px"} icon={<BiLock size={"20px"} />}>
                        <span>{t("Change Password")}</span>
                    </MenuItem>
                    <MenuItem iconSpacing={"5px"} icon={<RiUserReceived2Line size={"20px"} />}>
                        <span>{t("Logout")}</span>
                    </MenuItem>
                </MenuList>
            </Menu>
        </>
    );
}
