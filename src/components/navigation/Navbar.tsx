import { Box, Button, Flex, HStack } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { FaLanguage } from "react-icons/fa";
import { IoIosArrowBack, IoIosArrowForward } from "react-icons/io";
import { useDashboardState } from "../../context/DashboardContext";
import NotificationPanel from "./notification-control/NotificationPanel";
import { ProfileControlMenu } from "./ProfileControlMenu";
import SearchBar from "./SearchBar";

export default function NavBar() {
    const { i18n } = useTranslation();
    const [lang, setLang] = useState(localStorage.getItem("lang") || "en");
    const { isSideBarOpen, setIsSideBarOpen } = useDashboardState();

    const [screenSize, getDimension] = useState({
        dynamicWidth: window.innerWidth,
        dynamicHeight: window.innerHeight,
    });
    const setDimension = () => {
        getDimension({
            dynamicWidth: window.innerWidth,
            dynamicHeight: window.innerHeight,
        });
    };

    useEffect(() => {
        window.addEventListener("resize", setDimension);
        setIsSideBarOpen(screenSize.dynamicWidth > 1280);

        return () => {
            window.removeEventListener("resize", setDimension);
        };
    }, [screenSize]);

    const handleChangeLang = () => {
        const lang = localStorage.getItem("lang");

        if (lang == "en") {
            localStorage.setItem("lang", "ar");
        } else {
            localStorage.setItem("lang", "en");
        }
        setLang(localStorage.getItem("lang") || "en");
    };

    useEffect(() => {
        if (lang && lang != null) {
            i18n.changeLanguage(lang);
        }
    }, [lang]);

    const getArrow = () => {
        return lang === "ar" ? (
            <IoIosArrowBack
                cursor="pointer"
                onClick={() => {
                    setIsSideBarOpen(true);
                }}
            />
        ) : (
            <IoIosArrowForward
                cursor="pointer"
                onClick={() => {
                    setIsSideBarOpen(true);
                }}
            />
        );
    };

    return (
        <>
            <Box
                as="nav"
                marginTop="0"
                position="sticky"
                top="0"
                right="0"
                height="80px"
                backgroundColor="white"
                zIndex="1000">
                <Box paddingTop="20px" paddingBottom="10px">
                    <Flex justifyContent="space-between" alignContent="center">
                        <Box>
                            <HStack spacing={"4"}>
                                {!isSideBarOpen ? (
                                    <>
                                        {getArrow()}
                                        <SearchBar />
                                    </>
                                ) : (
                                    <SearchBar />
                                )}
                            </HStack>
                        </Box>
                        <HStack spacing="16px">
                            <NotificationPanel />
                            <Box>
                                <ProfileControlMenu />
                            </Box>
                            <Button
                                size={"lg"}
                                backgroundColor="brandLightGrey"
                                fontSize="14px"
                                fontWeight={600}
                                leftIcon={<FaLanguage />}
                                className="uppercase"
                                onClick={handleChangeLang}>
                                {i18n.language}
                            </Button>
                        </HStack>
                    </Flex>
                </Box>
                <div className="sm:hidden">
                    <div className="px-2 pt-2 pb-3 space-y-1"></div>
                </div>
            </Box>
        </>
    );
}
