import { ReactComponent as MailIcon } from "../../assets/navigation/mail.svg";
interface ClassProps {
    numberOfMails: number;
}
export function MailBadgeIcon({ numberOfMails }: ClassProps) {
    let numberOfMailsString = numberOfMails + "";
    if (numberOfMails > 99) {
        numberOfMailsString = numberOfMails + "+";
    }
    return (
        <>
            <span className="relative inline-block">
                <MailIcon style={{ width: "28px", height: "28px" }} />
                <span
                    hidden={numberOfMails == 0}
                    className="absolute top-0 right-0 inline-flex items-center justify-center px-2 py-1 text-xs font-bold leading-none text-red-100 transform translate-x-1/2 -translate-y-1/2 bg-red-600 rounded-full">
                    {numberOfMailsString}
                </span>
            </span>
        </>
    );
}
