import { useTranslation } from "react-i18next";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

export default function NavArrow() {
    const {
        i18n: { language },
    } = useTranslation();
    return <>
        {language === "ar" ? <FaArrowLeft className="text-sm" aria-hidden="true" /> : <FaArrowRight className="text-sm" aria-hidden="true" />}
    </>
}