import {Box} from "@chakra-ui/react";

interface ScrollableContainerProps {
    children: any;
    width: string;
    height: string;
    maxHeight: string;
    className?: string;
}
export default function ScrollableContainer(props: ScrollableContainerProps) {
    const {children, width, height, maxHeight, className} = props;
    return (
        <Box
            width={width}
            height={height}
            maxHeight={maxHeight}
            className={
                className
                    ? `overflow-x-hidden overflow-y-auto  ${className}`
                    : `overflow-x-hidden overflow-y-auto scrollableList`
            }>
            {children}
        </Box>
    );
}
