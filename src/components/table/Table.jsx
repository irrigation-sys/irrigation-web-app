import { Box, Heading, Spinner } from "@chakra-ui/react";
import React, { useEffect } from "react";
import { useTranslation } from "react-i18next";
import {
    FaAngleDoubleLeft,
    FaAngleDoubleRight,
    FaAngleLeft,
    FaAngleRight,
    FaSort,
    FaSortDown,
    FaSortUp,
} from "react-icons/fa";
import { useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from "react-table";
import { Button, GlobalFilterAdvancedSearch, PageButton } from "../Utils";
import { PlaceholderTable } from "./placeholder-message-table";
import "./Table.css";

const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {}, [resolvedRef, indeterminate]);

    return (
        <>
            <input
                className="w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-sm appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                type="checkbox"
                ref={resolvedRef}
                {...rest}
            />
        </>
    );
});

function Table({
    columns,
    data,
    searchPlaceholder,
    enableCreate = false,
    handleCreate = () => {},
    createText = "Add",
    setSelectedIds = id => {},
    isSelectable = false,
    stickyColumns = true,
    EmptyDataMessage = <PlaceholderTable />,
    isLoading = false,
}) {
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page, // Instead of using 'rows', we'll use page,
        // which has only the rows for the active page

        // The rest of these things are super handy, too ;)
        canPreviousPage,
        canNextPage,
        pageOptions,
        pageCount,
        gotoPage,
        nextPage,
        previousPage,
        setPageSize,
        state: { pageIndex, pageSize, globalFilter, selectedRowIds },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
        },
        useFilters, // useFilters!
        useGlobalFilter,
        useSortBy,
        usePagination, // new
        useRowSelect,
        hooks => {
            isSelectable &&
                hooks.allColumns.push(columns => [
                    // Let's make a column for selection
                    {
                        id: "selection",
                        // The header can use the table's getToggleAllRowsSelectedProps method
                        // to render a checkbox.  Pagination is a problem since this will select all
                        // rows even though not all rows are on the current page.  The solution should
                        // be server side pagination.  For one, the clients should not download all
                        // rows in most cases.  The client should only download data for the current page.
                        // In that case, getToggleAllRowsSelectedProps works fine.
                        Header: ({ getToggleAllRowsSelectedProps }) => (
                            <div>
                                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
                            </div>
                        ),
                        // The cell can use the individual row's getToggleRowSelectedProps method
                        // to the render a checkbox
                        Cell: ({ row }) => (
                            <div>
                                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                            </div>
                        ),
                    },
                    ...columns,
                ]);
        }
    );

    const {
        t,
        i18n: { language },
    } = useTranslation();

    useEffect(() => {
        isSelectable && setSelectedIds && setSelectedIds(Object.keys(selectedRowIds));
    }, [selectedRowIds]);

    // Render the UI for your table
    return (
        <div className="flex-1 space-y-8">
            <GlobalFilterAdvancedSearch
                createText={createText}
                enableCreate={enableCreate}
                globalFilter={globalFilter}
                handleCreate={handleCreate}
                headerGroups={headerGroups}
                preGlobalFilteredRows={preGlobalFilteredRows}
                searchPlaceholder={searchPlaceholder}
                setGlobalFilter={setGlobalFilter}
                setIsAdvancedSearchMode={() => {}}
                enableAdvancedSearch={false}
                enableFilter={!data.length && !isLoading ? false : true}
            />

            {/* table */}
            {data.length && !isLoading ? (
                <div className={`${stickyColumns ? "main-table" : ""} flex flex-col mt-4`}>
                    <div className="overflow-x-auto Flipped">
                        <div className="inline-block min-w-full px-1 py-2 align-middle ScrollContent">
                            {/* loading intecator */}

                            {/* display data here */}
                            <div className="border-b border-gray-200 shadow sm:rounded-md">
                                <table {...getTableProps()} className="min-w-full divide-y divide-gray-200">
                                    <thead style={{ backgroundColor: "rgba(78, 115, 248, 0.08)" }}>
                                        {headerGroups.map(headerGroup => (
                                            <tr {...headerGroup.getHeaderGroupProps()}>
                                                {headerGroup.headers.map(column => (
                                                    // Add the sorting props to control sorting. For this example
                                                    // we can add them into the header props
                                                    <th
                                                        scope="col"
                                                        className="px-6 py-3 text-sm font-semibold tracking-wider text-left text-black group"
                                                        {...column.getHeaderProps(column.getSortByToggleProps())}>
                                                        <div className="flex items-center justify-between">
                                                            {column.render("Header")}
                                                            {/* Add a sort direction indicator */}
                                                            <span>
                                                                {column.isSorted ? (
                                                                    column.isSortedDesc ? (
                                                                        <FaSortDown className="w-4 h-4 text-gray-400" />
                                                                    ) : (
                                                                        <FaSortUp className="w-4 h-4 text-gray-400" />
                                                                    )
                                                                ) : (
                                                                    <FaSort className="w-4 h-4 text-gray-400 opacity-0 group-hover:opacity-100" />
                                                                )}
                                                            </span>
                                                        </div>
                                                    </th>
                                                ))}
                                            </tr>
                                        ))}
                                    </thead>
                                    <tbody {...getTableBodyProps()} className="bg-white divide-y divide-gray-200">
                                        <>
                                            {page.length > 0 &&
                                                page.map((row, i) => {
                                                    // new
                                                    prepareRow(row);
                                                    return (
                                                        <tr
                                                            style={{ backgroundColor: "rgba(248, 248, 248,0.5)" }}
                                                            {...row.getRowProps()}
                                                            key={i}>
                                                            {row.cells.map(cell => {
                                                                return (
                                                                    <td
                                                                        {...cell.getCellProps()}
                                                                        className="px-6 py-4 whitespace-nowrap"
                                                                        role="cell">
                                                                        <div className="text-sm font-semibold text-gray-500">
                                                                            {cell.render("Cell")}
                                                                        </div>
                                                                    </td>
                                                                );
                                                            })}
                                                        </tr>
                                                    );
                                                })}
                                        </>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            ) : !data.length && !isLoading ? (
                <>{EmptyDataMessage}</>
            ) : isLoading ? (
                <Box
                    w="100%"
                    h="50vh"
                    display={"flex"}
                    flexDirection="column"
                    justifyContent={"center"}
                    alignItems="center">
                    <Heading fontSize={"18px"} fontWeight="600" marginY={6}>
                        {t("Please wait...")}
                    </Heading>
                    <Box>
                        <Spinner />
                    </Box>
                </Box>
            ) : null}

            {/* start pagination */}
            {page.length > 0 && !isLoading && (
                <div className="flex items-center justify-between text-sm">
                    <div className="flex justify-between flex-1 sm:hidden">
                        <Button onClick={() => previousPage()} disabled={!canPreviousPage}>
                            {t("Previous")}
                        </Button>
                        <Button onClick={() => nextPage()} disabled={!canNextPage}>
                            {t("Next")}
                        </Button>
                    </div>
                    <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                        <div className="flex items-baseline gap-x-2">
                            <span className="text-sm text-gray-700">
                                {t("Page")} <span className="font-medium">{pageIndex + 1}</span> {t("of")}{" "}
                                <span className="font-medium">{pageOptions.length}</span>
                            </span>
                            <label>
                                <span className="sr-only">{t("Items Per Page")}</span>
                                <select
                                    className="block w-full py-2 pl-3 pr-10 mt-1 text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                    value={pageSize}
                                    onChange={e => {
                                        setPageSize(Number(e.target.value));
                                    }}>
                                    {[5, 10, 20].map(pageSize => (
                                        <option key={pageSize} value={pageSize}>
                                            {t("Show")} {pageSize}
                                        </option>
                                    ))}
                                </select>
                            </label>
                        </div>
                        <div>
                            <nav
                                className="relative z-0 inline-flex -space-x-px text-sm rounded-md shadow-sm"
                                aria-label="Pagination">
                                <PageButton
                                    className={language === "ar" ? "rounded-r-md" : "rounded-l-md"}
                                    onClick={() => gotoPage(0)}
                                    disabled={!canPreviousPage}>
                                    <span className="sr-only">{t("First")}</span>
                                    {language === "ar" ? (
                                        <FaAngleDoubleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    ) : (
                                        <FaAngleDoubleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    )}
                                </PageButton>
                                <PageButton onClick={() => previousPage()} disabled={!canPreviousPage}>
                                    <span className="sr-only">Previous</span>
                                    {language === "ar" ? (
                                        <FaAngleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    ) : (
                                        <FaAngleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    )}
                                </PageButton>
                                <PageButton onClick={() => nextPage()} disabled={!canNextPage}>
                                    <span className="sr-only">{t("Next")}</span>
                                    {language === "ar" ? (
                                        <FaAngleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    ) : (
                                        <FaAngleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    )}
                                </PageButton>
                                <PageButton
                                    className={language === "ar" ? "rounded-l-md" : "rounded-r-md"}
                                    onClick={() => gotoPage(pageCount - 1)}
                                    disabled={!canNextPage}>
                                    <span className="sr-only">{t("Last")}</span>
                                    {language === "ar" ? (
                                        <FaAngleDoubleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    ) : (
                                        <FaAngleDoubleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                    )}
                                </PageButton>
                            </nav>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default Table;
