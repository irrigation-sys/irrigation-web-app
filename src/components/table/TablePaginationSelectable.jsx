import React, { useEffect, useMemo } from "react";
import { useTranslation } from "react-i18next";
import {
    FaAngleDoubleLeft,
    FaAngleDoubleRight,
    FaAngleLeft,
    FaAngleRight,
    FaFile,
    FaSort,
    FaSortDown,
    FaSortUp,
} from "react-icons/fa";
import { useFilters, useGlobalFilter, usePagination, useRowSelect, useSortBy, useTable } from "react-table";
import { Button, GlobalFilterAdvancedSearch, PageButton } from "./Utils";

const IndeterminateCheckbox = React.forwardRef(({ indeterminate, ...rest }, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    React.useEffect(() => {}, [resolvedRef, indeterminate]);

    return (
        <>
            <input
                className="form-check-input appearance-none h-4 w-4 border border-gray-300 rounded-sm bg-white checked:bg-blue-600 checked:border-blue-600 focus:outline-none transition duration-200 mt-1 align-top bg-no-repeat bg-center bg-contain float-left mr-2 cursor-pointer"
                type="checkbox"
                ref={resolvedRef}
                {...rest}
            />
        </>
    );
});

function TablePaginationSelectable({
    setPerPage,
    setPage,
    columns,
    data,
    currentPage,
    perPage,
    totalPage,
    searchPlaceholder,
    enableCreate = false,
    handleCreate = () => {},
    createText = "Add",
    isAdvancedSearchMode = false,
    setIsAdvancedSearchMode,
    enableAdvancedSearch = true,
    setSelectedIds,
    isSelectable = false,
}) {
    // Use the state and functions returned from useTable to build your UI
    const {
        getTableProps,
        getTableBodyProps,
        headerGroups,
        prepareRow,
        page,
        // canPreviousPage,
        // canNextPage,
        pageOptions,
        // pageCount,
        // gotoPage,
        // nextPage,
        // previousPage,
        // setPageSize,

        state: { pageIndex, pageSize, globalFilter, selectedRowIds },
        preGlobalFilteredRows,
        setGlobalFilter,
    } = useTable(
        {
            columns,
            data,
            useControlledState: state => {
                return useMemo(
                    () => ({
                        ...state,
                        pageIndex: currentPage,
                    }),
                    [state, currentPage]
                );
            },
            initialState: { pageIndex: currentPage },
            manualPagination: true,
            pageCount: totalPage,
        },
        useFilters, // useFilters!
        useGlobalFilter,
        useSortBy,
        usePagination, // new,
        useRowSelect,
        hooks => {
            isSelectable &&
                hooks.allColumns.push(columns => [
                    // Let's make a column for selection
                    {
                        id: "selection",
                        // The header can use the table's getToggleAllRowsSelectedProps method
                        // to render a checkbox.  Pagination is a problem since this will select all
                        // rows even though not all rows are on the current page.  The solution should
                        // be server side pagination.  For one, the clients should not download all
                        // rows in most cases.  The client should only download data for the current page.
                        // In that case, getToggleAllRowsSelectedProps works fine.
                        Header: ({ getToggleAllRowsSelectedProps }) => (
                            <div>
                                <IndeterminateCheckbox {...getToggleAllRowsSelectedProps()} />
                            </div>
                        ),
                        // The cell can use the individual row's getToggleRowSelectedProps method
                        // to the render a checkbox
                        Cell: ({ row }) => (
                            <div>
                                <IndeterminateCheckbox {...row.getToggleRowSelectedProps()} />
                            </div>
                        ),
                    },
                    ...columns,
                ]);
        }
    );

    const { t } = useTranslation();

    useEffect(() => {
        setSelectedIds && setSelectedIds(selectedRowIds);
    }, [selectedRowIds]);

    // Render the UI for your table
    return (
        <div className="space-y-8">
            <GlobalFilterAdvancedSearch
                createText={createText}
                enableCreate={enableCreate}
                globalFilter={globalFilter}
                handleCreate={handleCreate}
                headerGroups={headerGroups}
                enableFilter={isAdvancedSearchMode}
                preGlobalFilteredRows={preGlobalFilteredRows}
                searchPlaceholder={searchPlaceholder}
                setGlobalFilter={setGlobalFilter}
                setIsAdvancedSearchMode={setIsAdvancedSearchMode}
                enableAdvancedSearch={enableAdvancedSearch}
            />
            {/* table */}
            <div className="flex flex-col mt-4">
                <div className="-mx-4 -my-2 overflow-x-auto sm:-mx-6 lg:-mx-8 Flipped">
                    <div className="inline-block min-w-full py-2 align-middle sm:px-6 ScrollContent">
                        <div className="overflow-hidden border-b border-gray-200 shadow sm:rounded-md">
                            <table {...getTableProps()} className="min-w-full divide-y divide-gray-200">
                                <thead className="bg-gray-100">
                                    {headerGroups.map(headerGroup => (
                                        <tr {...headerGroup.getHeaderGroupProps()}>
                                            {headerGroup.headers.map(column => (
                                                // Add the sorting props to control sorting. For this example
                                                // we can add them into the header props
                                                <th
                                                    scope="col"
                                                    className="px-6 py-3 text-sm font-semibold tracking-wider text-left text-black group"
                                                    {...column.getHeaderProps(column.getSortByToggleProps())}>
                                                    <div className="flex items-center justify-between">
                                                        {column.render("Header")}
                                                        {/* Add a sort direction indicator */}
                                                        <span>
                                                            {column.isSorted ? (
                                                                column.isSortedDesc ? (
                                                                    <FaSortDown className="w-4 h-4 text-gray-400" />
                                                                ) : (
                                                                    <FaSortUp className="w-4 h-4 text-gray-400" />
                                                                )
                                                            ) : (
                                                                <FaSort className="w-4 h-4 text-gray-400 opacity-0 group-hover:opacity-100" />
                                                            )}
                                                        </span>
                                                    </div>
                                                </th>
                                            ))}
                                        </tr>
                                    ))}
                                </thead>
                                <tbody {...getTableBodyProps()} className="bg-white divide-y divide-gray-200">
                                    {(page.length > 0 &&
                                        page.map((row, i) => {
                                            // new
                                            prepareRow(row);
                                            return (
                                                <tr {...row.getRowProps()}>
                                                    {row.cells.map(cell => {
                                                        return (
                                                            <td
                                                                {...cell.getCellProps()}
                                                                className="px-6 py-4 whitespace-nowrap"
                                                                role="cell">
                                                                <div className="text-sm font-semibold text-gray-500">
                                                                    {cell.render("Cell")}
                                                                </div>
                                                            </td>
                                                        );
                                                    })}
                                                </tr>
                                            );
                                        })) || (
                                        <tr>
                                            <td
                                                colSpan={columns.length}
                                                className="items-center content-center justify-center text-center">
                                                <FaFile />
                                                <span className="px-3 text-sm text-black">{t("No data found")}</span>
                                            </td>
                                        </tr>
                                    )}
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            {page.length > 0 && (
                <div className="flex items-center justify-between text-sm">
                    <div className="flex justify-between flex-1 sm:hidden">
                        <Button onClick={() => setPage(s => (s === 0 ? 0 : s - 1))} disabled={currentPage === 0}>
                            {t("Previous")}
                        </Button>
                        <Button onClick={() => setPage(s => s + 1)} disabled={currentPage === totalPage - 1}>
                            {t("Next")}
                        </Button>
                    </div>
                    <div className="hidden sm:flex-1 sm:flex sm:items-center sm:justify-between">
                        <div className="flex items-baseline gap-x-2">
                            <span className="text-sm text-gray-700">
                                {t("Page")} <span className="font-medium">{pageIndex + 1}</span> {t("of")}{" "}
                                <span className="font-medium">{pageOptions.length}</span>
                            </span>
                            <label>
                                <span className="sr-only">{t("Items Per Page")}</span>
                                <select
                                    className="block w-full py-2 pl-3 pr-10 mt-1 text-sm border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                                    value={perPage}
                                    onChange={e => {
                                        setPerPage(Number(e.target.value));
                                    }}>
                                    {[5, 10, 20].map(pageSize => (
                                        <option key={pageSize} value={pageSize}>
                                            {t("Show")} {pageSize}
                                        </option>
                                    ))}
                                </select>
                            </label>
                        </div>
                        <div>
                            <nav
                                className="relative z-0 inline-flex -space-x-px text-sm rounded-md shadow-sm"
                                aria-label="Pagination">
                                <PageButton
                                    className="rounded-l-md"
                                    onClick={() => setPage(0)}
                                    disabled={currentPage === 0}>
                                    <span className="sr-only">{t("First")}</span>
                                    <FaAngleDoubleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                </PageButton>
                                <PageButton
                                    onClick={() => setPage(s => (s === 0 ? 0 : s - 1))}
                                    disabled={currentPage === 0}>
                                    <span className="sr-only">{t("Previous")}</span>
                                    <FaAngleLeft className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                </PageButton>
                                <PageButton
                                    onClick={() => setPage(s => s + 1)}
                                    disabled={currentPage === totalPage - 1}>
                                    <span className="sr-only">{t("Next")}</span>
                                    <FaAngleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                </PageButton>
                                <PageButton
                                    className="rounded-r-md"
                                    onClick={() => setPage(totalPage - 1)}
                                    disabled={currentPage === totalPage - 1}>
                                    <span className="sr-only">{t("Last")}</span>
                                    <FaAngleDoubleRight className="w-4 h-4 text-gray-400" aria-hidden="true" />
                                </PageButton>
                            </nav>
                        </div>
                    </div>
                </div>
            )}
        </div>
    );
}

export default TablePaginationSelectable;
