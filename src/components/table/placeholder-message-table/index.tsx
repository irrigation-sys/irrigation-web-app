import { Box, Flex } from "@chakra-ui/react";
import { ReactNode } from "react";
import { ReactComponent as NoContent } from "../../../assets/no-content.svg";
import "./style.css";
type PlaceholderTableProps = {
    Icon: ReactNode;
    heading?: string;
    message?: string;
    children?: ReactNode;
};

export function PlaceholderTable({ Icon, heading, message, children }: PlaceholderTableProps) {
    return (
        <Flex
            display={"flex"}
            flexDirection={"column"}
            alignItems="center"
            justifyContent={"center"}
            className="placeholder__message-table">
            <Box>{Icon || <NoContent />}</Box>
            <Box color={"gray.900"} fontWeight={600} fontSize={14}>
                <h2>{heading || "No Data Found On The System "}</h2>
            </Box>
            {message ? (
                <Box color={"gray.600"} fontWeight={500} fontSize={12}>
                    <span>{message}</span>
                </Box>
            ) : null}
            {children}
        </Flex>
    );
}
