interface ErrorMessage {
    message?: string;
}

export function ErrorMessage({ message }: ErrorMessage) {
    return <label className={"req_label ltr:float-right rtl:float-left"}>{message}</label>;
}
