import { Box, Button as ChakraButton, Flex, Image } from "@chakra-ui/react";
import moment from "moment-timezone";
import "moment/dist/locale/ar";
import { useMemo, useState } from "react";
import { useTranslation } from "react-i18next";
import { FaPlus, FaSearch } from "react-icons/fa";
import { useAsyncDebounce } from "react-table";
import advancedSearchImage from "../assets/search-status.png";
import { UserRole, YearSemesterStatus } from "../models/enums/enums";

export function classNames(...classes) {
    return classes.filter(Boolean).join(" ");
}

export function Button({ children, className, ...rest }) {
    return (
        <button
            type="button"
            className={classNames(
                "relative inline-flex items-center px-4 py-2 border border-gray-300 text-sm font-medium rounded-md text-gray-700 bg-white hover:bg-gray-50",
                className
            )}
            {...rest}>
            {children}
        </button>
    );
}

export function PageButton({ children, className, ...rest }) {
    return (
        <button
            type="button"
            className={classNames(
                "relative inline-flex items-center px-2 py-2 border border-gray-300 bg-white text-sm font-medium text-gray-500 hover:bg-gray-50",
                className
            )}
            {...rest}>
            {children}
        </button>
    );
}

export function GlobalFilter({
    preGlobalFilteredRows,
    globalFilter,
    setGlobalFilter,
    searchPlaceholder,
    enableCreate,
    handleCreate,
    createText,
    setIsAdvancedSearchMode,
    enableAdvancedSearch,
    enableFilter,
}) {
    const count = preGlobalFilteredRows.length;
    const [value, setValue] = useState(globalFilter);
    const onChange = useAsyncDebounce(value => {
        setGlobalFilter(value || undefined);
    }, 200);
    const {
        t,
        i18n: { language },
    } = useTranslation();

    const getIconStyle = () => {
        if (language === "ar") {
            return "absolute top-3 right-3";
        }
        return "absolute top-3 left-3";
    };

    return (
        <div className="flex items-center justify-between">
            <div className="relative">
                {enableFilter && (
                    <Flex>
                        <div className={getIconStyle()}>
                            <FaSearch />
                        </div>
                        <input
                            type="text"
                            className="py-3 text-sm border-gray-300 rounded-md ps-10 pe-3 focus:outline-none"
                            style={{ minWidth: 290 }}
                            value={value || ""}
                            onChange={e => {
                                setValue(e.target.value);
                                onChange(e.target.value);
                            }}
                            placeholder={searchPlaceholder}
                        />
                        {enableAdvancedSearch && (
                            <Box alignSelf="center">
                                <ChakraButton
                                    onClick={() => {
                                        setIsAdvancedSearchMode(true);
                                    }}
                                    ms="10"
                                    py="6"
                                    leftIcon={<Image src={advancedSearchImage} alt="Advanced Search" />}
                                    color="brandLightBrand"
                                    variant="solid">
                                    <span>{t(`Advanced Search`)}</span>
                                </ChakraButton>
                            </Box>
                        )}
                    </Flex>
                )}
            </div>

            {enableCreate && (
                <button style={{ width: "auto" }} className="text-sm btn-form-primary" onClick={handleCreate}>
                    <FaPlus className="me-0 md:me-3" />
                    <span style={{ minWidth: "140px" }} className="hidden md:inline-block">
                        {createText}
                    </span>
                </button>
            )}
        </div>
    );
}

// This is a custom filter UI for selecting
// a unique option from a list
export function SelectColumnFilter({ column: { filterValue, setFilter, preFilteredRows, id, render } }) {
    // Calculate the options for filtering
    // using the preFilteredRows
    const options = useMemo(() => {
        const options = new Set();
        preFilteredRows.forEach(row => {
            options.add(row.values[id]);
        });
        return [...options.values()];
    }, [id, preFilteredRows]);

    // Render a multi-select box
    return (
        <label className="flex items-baseline gap-x-2">
            <span className="text-gray-700">{render("Header")}: </span>
            <select
                className="border-gray-300 rounded-md shadow-sm focus:border-indigo-300 focus:ring focus:ring-indigo-200 focus:ring-opacity-50"
                name={id}
                id={id}
                value={filterValue}
                onChange={e => {
                    setFilter(e.target.value || undefined);
                }}>
                <option value="">All</option>
                {options.map((option, i) => (
                    <option key={i} value={option}>
                        {option}
                    </option>
                ))}
            </select>
        </label>
    );
}

export function GlobalFilterAdvancedSearch({
    preGlobalFilteredRows,
    globalFilter,
    setGlobalFilter,
    searchPlaceholder,
    enableCreate,
    handleCreate,
    createText,
    setIsAdvancedSearchMode,
    headerGroups,
    enableAdvancedSearch,
    enableFilter,
}) {
    return (
        <div className="lg:block sm:block sm:gap-x-2">
            <GlobalFilter
                preGlobalFilteredRows={preGlobalFilteredRows}
                globalFilter={globalFilter}
                setGlobalFilter={setGlobalFilter}
                searchPlaceholder={searchPlaceholder}
                enableCreate={enableCreate}
                handleCreate={handleCreate}
                createText={createText}
                enableAdvancedSearch={enableAdvancedSearch}
                setIsAdvancedSearchMode={setIsAdvancedSearchMode}
                enableFilter={enableFilter}
            />

            {headerGroups.map(headerGroup =>
                headerGroup.headers.map(column =>
                    column.Filter ? (
                        <div className="mt-2 sm:mt-0" key={column.id}>
                            {column.render("Filter")}
                        </div>
                    ) : null
                )
            )}
        </div>
    );
}

export function StatusPill({ value: status }) {
    const { t } = useTranslation();
    return (
        <span
            className={classNames(
                "px-3 py-1 leading-wide font-bold text-xs rounded-full shadow-sm",
                status ? "bg-green-100 text-green-800" : null,
                !status ? "bg-yellow-100 text-yellow-950" : null
            )}>
            {status ? t("Active") : t("Inactive")}
        </span>
    );
}

export function YearSemesterStatusPill({ value: status }) {
    const { t } = useTranslation();
    return (
        <span
            className={classNames(
                "px-3 py-1 leading-wide font-bold text-xs rounded-full shadow-sm",
                status == YearSemesterStatus.CURRENT ? "bg-green-100 text-green-800" : null,
                status == YearSemesterStatus.NOT_STARTED ? "bg-yellow-100 text-yellow-950" : null,
                status == YearSemesterStatus.ENDED ? "bg-gray-100 text-gray-400" : null
            )}>
            {t(status)}
        </span>
    );
}

export function Translate({ value }) {
    const { t } = useTranslation();
    return <span>{t(value)}</span>;
}

export function RemoveTime({ value: date }) {
    return <span>{date ? date.substring(0, 10) : ""}</span>;
}

export function AvatarCell({ value, column, row }) {
    return (
        <div className="flex items-center">
            <div className="flex-shrink-0 w-10 h-10">
                <img className="w-10 h-10 rounded-full" src={row.original[column.imgAccessor]} alt="" />
            </div>
            <div className="ml-4">
                <div className="text-sm font-medium text-gray-900">{value}</div>
                <div className="text-sm text-gray-500">{row.original[column.emailAccessor]}</div>
            </div>
        </div>
    );
}

export function isAdmin(roles) {
    return (
        roles &&
        (roles.includes(UserRole.SYSTEM_ADMIN) ||
            roles.includes(UserRole.PlotLand_GROUP_ADMIN) ||
            roles.includes(UserRole.PlotLand_ADMIN) ||
            roles.includes(UserRole.STAGE_ADMIN))
    );
}

export function transformDate(date, language) {
    try {
        if (date === 0) {
            return "";
        }
        if (language == "ar") {
            moment.locale("ar");
        } else {
            moment.locale("en");
        }
        return moment.tz(date, "YYYY-MM-DD'T'HH:mm:ss.SSSZ", "Africa/Cairo").local().startOf("seconds").fromNow();
    } catch (error) {
        console.log(error);
        return "";
    }
}

export function formatDateTime(date, language) {
    try {
        if (date === 0) {
            return "";
        }
        if (language == "ar") {
            moment.locale("ar");
        } else {
            moment.locale("en");
        }
        return moment.utc(date).format("YYYY / MM / DD - hh:mm A");
    } catch (error) {
        console.log(error);
        return "";
    }
}

export function getFileExtension(fileName) {
    if (fileName) {
        let extension = fileName.split(".").pop();
        if (extension && extension.includes("_")) {
            extension = extension.split("_")[0];
        }
        return extension ? extension.toLowerCase() : extension;
    }
    return "";
}

export function formatDate(date) {
    if (date) {
        let dateStr = date.toString();
        if (dateStr && dateStr.length > 10) dateStr = dateStr.substring(0, 10);
        return dateStr.replaceAll("-", " / ");
    }
    return "";
}
