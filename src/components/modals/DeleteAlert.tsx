import {
    AlertDialog,
    AlertDialogBody,
    AlertDialogContent,
    AlertDialogFooter,
    AlertDialogHeader,
    AlertDialogOverlay,
    Button,
} from "@chakra-ui/react";
import { useRef } from "react";
import { FaArchive } from "react-icons/fa";

type DeleteAlertType = {
    header: string;
    body: string;
    noText: string;
    yesText: string;
    isOpen: boolean;
    setIsOpen: (open: boolean) => void;
    handleDelete: () => void;
    isLoading?: boolean;
};
function DeleteAlert({ header, body, noText, yesText, isOpen, setIsOpen, handleDelete, isLoading }: DeleteAlertType) {
    const onClose = () => setIsOpen(false);
    const cancelRef = useRef<HTMLButtonElement>(null);

    return (
        <AlertDialog isOpen={isOpen} onClose={onClose} leastDestructiveRef={cancelRef}>
            <AlertDialogOverlay>
                <AlertDialogContent>
                    <AlertDialogHeader fontSize="18" fontWeight="semibold">
                        <FaArchive className="text-gray-400 me-3" />
                        {header}
                    </AlertDialogHeader>

                    <AlertDialogBody fontSize="16">{body}</AlertDialogBody>

                    <AlertDialogFooter>
                        <Button
                            border={"1px"}
                            borderRadius="8"
                            fontSize="14"
                            borderColor={"brandBlue"}
                            color="brandBlue"
                            fontWeight={"semibold"}
                            height={"39px"}
                            width={"112px"}
                            backgroundColor="white"
                            ref={cancelRef}
                            onClick={onClose}>
                            {noText}
                        </Button>
                        <Button
                            fontSize="14"
                            borderRadius="8"
                            height={"39px"}
                            width={"112px"}
                            isLoading={isLoading}
                            fontWeight={"semibold"}
                            backgroundColor="brandLightRed"
                            _hover={{ bg: "brandLightRed" }}
                            color={"white"}
                            onClick={handleDelete}
                            ms={3}>
                            {yesText}
                        </Button>
                    </AlertDialogFooter>
                </AlertDialogContent>
            </AlertDialogOverlay>
        </AlertDialog>
    );
}

export default DeleteAlert;
