import {
    Button,
    Modal,
    ModalBody,
    ModalCloseButton,
    ModalContent,
    ModalFooter,
    ModalHeader,
    ModalOverlay,
} from "@chakra-ui/react";
import React from "react";

type CancelModalType = {
    header: string;
    body: string;
    noText: string;
    yesText: string;
    onClose: () => void;
    onSubmit: () => void;
    isOpen: boolean;
};

function CancelModal({header, body, noText, yesText, isOpen, onClose, onSubmit}: CancelModalType) {
    return (
        <Modal isOpen={isOpen} onClose={onClose}>
            <ModalOverlay />
            <ModalContent>
                <ModalHeader fontSize="18" fontWeight="600">
                    {header}
                </ModalHeader>
                <ModalCloseButton />
                <ModalBody fontSize="16" fontWeight="600">
                    {body}
                </ModalBody>

                <ModalFooter>
                    <Button fontSize="14" fontWeight="600" variant="ghost" mr={3} onClick={onClose}>
                        {noText}
                    </Button>
                    <Button fontSize="14" fontWeight="600" className="skv-btn-primary" onClick={onSubmit}>
                        {yesText}
                    </Button>
                </ModalFooter>
            </ModalContent>
        </Modal>
    );
}

export default CancelModal;
