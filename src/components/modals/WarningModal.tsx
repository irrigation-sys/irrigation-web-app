import { Box, HStack, Text } from "@chakra-ui/layout";
import { ListItem, UnorderedList, VStack } from "@chakra-ui/react";
import { FaPollH } from "react-icons/fa";

interface MessageAlertProps {
    titleMessage?: string;
    warningMessages?: string[];
}

export function WarningAlert({ titleMessage, warningMessages }: MessageAlertProps) {
    return (
        <Box bg="rgba(78, 115, 248, 0.02)" w="100%"  pt= {8} borderRadius="8px">
            <VStack align='left'>    
                <HStack pl={2}>
                    <FaPollH className="mr-2" color="#FCA549" /> <Text fontWeight="600" fontSize="14" color="black">{titleMessage}</Text>
                </HStack>
                <UnorderedList pl={14} fontSize="13">
                        {warningMessages?.map(message => <ListItem>{message}</ListItem>)}
                </UnorderedList>
            </VStack>
        </Box>
    )
}