import { useTranslation } from "react-i18next";
import { FaArrowLeft, FaArrowRight } from "react-icons/fa";

type ActionArrowProps = {
    color?: string;
    onClick: () => void;
    dir?: "forward" | "backward"
}

export default function ActionArrow({ color, onClick, dir }: ActionArrowProps) {
    const localdir = dir || "forward"
    const {
        i18n: { language },
    } = useTranslation();
    return <>
        {
            localdir === "forward" ?
                (language === "ar" ?
                    <FaArrowLeft color={color || ""} onClick={onClick} className="text-sm" aria-hidden="true" cursor={"pointer"} /> :
                    <FaArrowRight onClick={() => onClick || {}} color={color} className="text-sm" aria-hidden="true" cursor={"pointer"} />
                ) :
                (language === "ar" ?
                    <FaArrowRight onClick={() => onClick || {}} color={color} className="text-sm" aria-hidden="true" cursor={"pointer"} /> :
                    <FaArrowLeft color={color || ""} onClick={onClick} className="text-sm" aria-hidden="true" cursor={"pointer"} />
                )
        }
    </>
}