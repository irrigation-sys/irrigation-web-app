import { Box, HStack } from "@chakra-ui/layout";
import { useEffect, useState } from "react";
import { FaCheckCircle, FaExclamationCircle } from "react-icons/fa";

interface MessageAlertProps {
    message?: string;
}

export function SuccessAlert({ message }: MessageAlertProps) {
    const [success, setSuccess] = useState<any>("");

    useEffect(() => {
        window.scrollTo(0, 0);
    }, [message]);

    useEffect(() => {
        setSuccess(message);
        setTimeout(() => {
            setSuccess("");
        }, 5000);
    }, [message]);

    return (
        <>
            {success && (
                <Box bg="brandHollowGreen" w="100%" p={4} color="brandGreen" borderRadius="8px">
                    <HStack>
                        <FaCheckCircle className="mr-4" />
                        <span>{success}</span>
                    </HStack>
                </Box>
            )}
        </>
    );
}

export function ErrorAlert({ message = "" }: MessageAlertProps) {
    const [error, setError] = useState<string>("");

    useEffect(() => {
        window.scrollTo(0, 0);
    }, []);

    useEffect(() => {
        setError(message);

        setTimeout(() => {
            setError("");
        }, 5000);
    }, [message]);

    return (
        <>
            {error ? (
                <Box bg="brandHollowRed" w="100%" p={4} color="brandRed" borderRadius="8px">
                    <HStack>
                        <FaExclamationCircle className="mr-4" />
                        <span>{error}</span>
                    </HStack>
                </Box>
            ) : null}
        </>
    );
}
