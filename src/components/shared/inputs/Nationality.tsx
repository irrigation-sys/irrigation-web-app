import { Box, FormLabel } from "@chakra-ui/react";
import { AutoComplete, AutoCompleteInput, AutoCompleteItem, AutoCompleteList } from "@choc-ui/chakra-autocomplete";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { ErrorMessage } from "../FormHelpers";
import RedStar from "./RedStar";

interface NationalityProps {
    formControl: any;
    error: any;
    isReqStar?: boolean;
}

const nationalities = [
    {
        value: "Egyptian",
        en: "Egyptian",
        ar: "مصري",
    },
    {
        value: "Saudi",
        en: "Saudi",
        ar: "سعودي",
    },
    {
        value: "Emirati",
        en: "Emirati",
        ar: "إماراتي",
    },
];

const Nationality = ({ formControl, error, isReqStar }: NationalityProps) => {
    const [value, setValue] = useState(formControl?.value || "");
    const {
        t,
        i18n: { language },
    } = useTranslation();
    useEffect(() => {
        setValue(formControl.value);
    }, [formControl.value]);

    function isEmpty(strValue: any) {
        if (!strValue || strValue.trim() === "" || (strValue.trim()).length === 0) {
            return true;
        }
        return false;
    }

    return (
        <Box>
            <FormLabel fontWeight="600" fontSize={"0.875rem"} as="legend">
                {t("Nationality")}{isReqStar && <RedStar />}
                {error && <ErrorMessage message={error.message} />}
            </FormLabel>
            <div key={value}>
                <AutoComplete openOnFocus
                    defaultValue={value} onChange={(vals) => {
                        setValue(vals)
                        formControl.onChange && formControl.onChange(vals);
                    }}
                >
                    <AutoCompleteInput
                        size="lg"
                        fontSize="12"
                        autoComplete="off"
                        variant="outline"
                        placeholder={t("Search...")}
                        onChange={(vals) => {
                            if (isEmpty(vals?.target.value)) {
                                setValue(null)
                                formControl.onChange && formControl.onChange(null);
                            }
                        }}
                    />
                    <AutoCompleteList>
                        {nationalities.map((option, oid) => (
                            <AutoCompleteItem
                                key={`option-${oid}`}
                                value={option.value}
                                label={language == "ar" ? option.ar : option.en}
                                textTransform="capitalize">
                                {language === "en" ? option.en : option.ar}
                            </AutoCompleteItem>
                        ))}
                    </AutoCompleteList>
                </AutoComplete>
            </div>
        </Box>
    );
};

export default Nationality;
