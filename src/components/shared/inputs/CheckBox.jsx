import React from "react";

const Checkbox = React.forwardRef(({rest}, ref) => {
    const defaultRef = React.useRef();
    const resolvedRef = ref || defaultRef;

    // React.useEffect(() => {}, [resolvedRef, indeterminate]);

    return (
        <>
            <input
                className="w-4 h-4 mt-1 mr-2 align-top transition duration-200 bg-white bg-center bg-no-repeat bg-contain border border-gray-300 rounded-sm appearance-none cursor-pointer form-check-input checked:bg-blue-600 checked:border-blue-600 focus:outline-none"
                type="checkbox"
                ref={resolvedRef}
                {...rest}
            />
        </>
    );
});
