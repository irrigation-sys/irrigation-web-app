import { chakra } from "@chakra-ui/react";

export default function RedStar() {
    return (
        <chakra.span color="black" ml="1" mr="1">
            *
        </chakra.span>
    );
}
