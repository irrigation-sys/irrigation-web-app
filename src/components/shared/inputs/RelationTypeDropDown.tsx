import { RelationType } from "../../../models/enums/enums";
import { DropDownOption } from "../../../models/inputs/DropDownOption";
import DropDown from "./base/DropDown";

interface StaffTypeDropDown {
    formControl: any;
    label: string;
    error?: any;
    isReqStar?: boolean;
}
const relationTypes: DropDownOption[] = [
    {
        value: RelationType.MOTHER,
        displayAr: "أم",
        displayEn: "Mother",
    },
    {
        value: RelationType.FATHER,
        displayAr: "أب",
        displayEn: "Father",
    },
    {
        value: RelationType.RELATIVE,
        displayAr: "قريب عائلي",
        displayEn: "Relative",
    },
];
export default function RelationTypeDropDown({ formControl, label, error, isReqStar }: StaffTypeDropDown) {
    const isReqStarDropDown = isReqStar || false
    return <DropDown isReqStar={isReqStarDropDown} options={relationTypes} formControl={formControl} label={label} error={error} />;
}
