import { Box, FormLabel } from "@chakra-ui/react";
import { AutoComplete, AutoCompleteInput, AutoCompleteItem, AutoCompleteList } from "@choc-ui/chakra-autocomplete";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { DropDownOption } from "../../../../models/inputs/DropDownOption";
import { ErrorMessage } from "../../FormHelpers";

interface DropDownProps {
    formControl: any;
    options: DropDownOption[];
    label: string;
    error?: any;
}

export default function DropDownSearch({ formControl, options, label, error }: DropDownProps) {
    const [value, setValue] = useState(formControl?.value || "");
    const {
        t,
        i18n: { language },
    } = useTranslation();
    useEffect(() => {
        setValue(formControl.value);
    }, [formControl.value]);

    function isEmpty(strValue: any) {
        if (!strValue || strValue.trim() === "" || (strValue.trim()).length === 0) {
            return true;
        }
        return false;
    }

    return (
        <Box>
            <FormLabel fontWeight="700" as="legend">
                {t(`${label}`)}
                {error && <ErrorMessage message={error.message} />}
            </FormLabel>
            <div key={value}>
                <AutoComplete openOnFocus
                    defaultValue={value} onChange={(vals) => {
                        setValue(vals)
                        formControl.onChange && formControl.onChange(vals);
                    }}
                >
                    <AutoCompleteInput
                        size="lg"
                        fontSize="12"
                        autoComplete="off"
                        variant="outline"
                        placeholder={t("Search...")}
                        onChange={(vals) => {
                            if (isEmpty(vals?.target.value)) {
                                setValue(null)
                                formControl.onChange && formControl.onChange(null);
                            }
                        }}
                    />
                    <AutoCompleteList>
                        {options.map((option, oid) => (
                            <AutoCompleteItem
                                key={`option-${oid}`}
                                value={option.value}
                                label={language == "ar" ? option.displayEn : option.displayAr}
                                textTransform="capitalize">
                                {language === "en" ? option.displayEn : option.displayAr}
                            </AutoCompleteItem>
                        ))}
                    </AutoCompleteList>
                </AutoComplete>
            </div>
        </Box>
    );
}