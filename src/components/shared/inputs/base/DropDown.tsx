import { Box, FormLabel, Select } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useTranslation } from "react-i18next";
import { DropDownOption } from "../../../../models/inputs/DropDownOption";
import { ErrorMessage } from "../../../FormHelpers";
import RedStar from "../RedStar";

interface DropDownProps {
    formControl: any;
    options: DropDownOption[];
    label: string;
    error?: any;
    isReqStar?: boolean;
}

export default function DropDown({ formControl, options, label, error, isReqStar }: DropDownProps) {
    const [value, setValue] = useState(formControl?.value || "");
    const {
        t,
        i18n: { language },
    } = useTranslation();
    useEffect(() => {
        setValue(formControl.value);
    }, [formControl.value]);
    return (
        <Box>
            <FormLabel fontWeight="600" fontSize="14" as="legend">
                {t(`${label}`)}
                {isReqStar && <RedStar />}
                {error && <ErrorMessage message={error.message} />}
            </FormLabel>
            <div key={value}>
                <Select
                    dir={language === "ar" ? "rtl" : "ltr"}
                    fontSize="12"
                    size="lg"
                    placeholder={t("Select option")}
                    backgroundColor="white"
                    defaultValue={value}
                    disabled={formControl.disabled}
                    onChange={vals => {
                        setValue(vals);
                        formControl.onChange && formControl.onChange(vals.target.value);
                    }}
                    isInvalid={error ? true : false}
                    errorBorderColor="red.300"
                    isDisabled={formControl.disabled}>
                    {options.map((option, oid) => (
                        <option key={oid} value={option.value}>
                            {language === "en" ? option.displayEn : option.displayAr}
                        </option>
                    ))}
                </Select>
            </div>
        </Box>
    );
}
