import { PropsWithChildren } from "react";
import { useHistory } from "react-router-dom";

type Props = {
    name: string;
};

export const useRouteState = <T extends unknown>(props: PropsWithChildren<Props>) => {
    const history = useHistory();
    const { state } = history.location;
    const { name } = props;

    const getRouteState = () => {
        let value;
        try {
            value = (state as any)[`${name}`] as T;
        } catch {
            history.push("/home");
            return;
        }
        if (!value) {
            history.push("/home");
            return;
        }
        return value;
    };

    return { getRouteState };
};
