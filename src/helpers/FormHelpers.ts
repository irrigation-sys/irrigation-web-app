export const isArabic = (e: string) => {
    const r = /^[a-zA-Z0-9]+$/;
    let i = e.length;
    while (--i) {
        if (r.test(e[i])) {
            return false;
        }
    }
    return true;
};

export const isArabicNotRequired = (e: string) => {
    if (!e) {
        return true;
    }
    const r = /^[a-zA-Z0-9]+$/;
    let i = e.length;
    while (--i) {
        if (r.test(e[i])) {
            return false;
        }
    }
    return true;
};
