const lessThan10MB = (files: File[]) => {
    return files[0]?.size < 10000000;
};

const lessThan600MB = (files: File[]) => {
    return files[0]?.size < 600000000;
};

const acceptedFormats = (files: File[]) => {
    return ["image/jpeg", "image/png", "image/gif"].includes(files[0]?.type);
};

export const validateImage = (files: File[]) => {
    if (!files || files.length < 0 || lessThan10MB(files) || acceptedFormats(files)) {
        return true;
    }
};

export function getPathName(path: string) {
    const u = new URL(path);
    u.hash = "";
    u.search = "";
    return u.toString();
}

const acceptedFileFormats = (files: File[]) => {
    return [
        "image/jpeg",
        "image/png",
        "image/gif",
        "application/pdf",
        "application/msword",
        "text/plain",
        "image/tiff",
        "text/csv",
        "application/vnd.ms-excel",
        "text/plain",
    ].includes(files[0]?.type);
};

export const validateFile = (files: File[]) => {
    if (!files || files.length < 0 || lessThan10MB(files) || acceptedFileFormats(files)) {
        return true;
    } else {
        return false;
    }
};

export const validateVideo = (files: File[]) => {
    if (!files || files.length < 0 || lessThan600MB(files)) {
        return true;
    } else {
        return false;
    }
};

export const validateMaterial = (files: File[]) => {
    const acceptedExtensions = [
        "doc",
        "docx",
        "ppt",
        "pptx",
        "pdf",
        "xls",
        "rar",
        "zip",
        "pps",
        "ppsx",
        "ibooks",
        "html",
        "xlsx",
        "SCORM",
    ];
    if (
        !files ||
        files.length < 0 ||
        (lessThan600MB(files) && acceptedExtensions.includes(files[0].name.split(".").pop()!))
    ) {
        return true;
    } else {
        return false;
    }
};
