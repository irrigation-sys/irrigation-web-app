import { createContext, useContext } from 'react';

export const authContext = createContext<any>(null);
export function useAuth() {
    return useContext(authContext);
}
