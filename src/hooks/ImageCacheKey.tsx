import { useEffect, useState } from 'react';

export const useImageCacheKey = () => {
    const [imgRenderCount, setImgRenderCount] = useState(0);
    const [imgKey, setImgKey] = useState("");


    useEffect(() => {
        setImgKey(Date.now().toLocaleString());
    }, [])

    const updateImageCacheKey = () => {
        setImgRenderCount(
            imgRenderCount + 1
        );
    }

    const getImageCacheKey = () => {
        return imgKey + imgRenderCount;
    }

    return {
        updateImageCacheKey,
        getImageCacheKey
    };

}