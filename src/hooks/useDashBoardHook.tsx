import { useState } from "react";

export default function useDashBoardHook() {
    const [isSideBarOpen, setIsSideBarOpen] = useState(true);
    const [selectedPageIndex, setSelectedPageIndex] = useState<number | null>(null);
    const [selectedSubPageIndex, setSelectedSubPageIndex] = useState<number | null>(null);
    return {
        isSideBarOpen,
        setIsSideBarOpen,
        selectedPageIndex,
        selectedSubPageIndex,
        setSelectedPageIndex,
        setSelectedSubPageIndex
    }

}