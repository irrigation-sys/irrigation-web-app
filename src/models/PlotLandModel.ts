export interface PlotLandModel {
    id: string;
    name: string;
    code: string;
    waterAmount: number;
    agriculturalCrop: string;
    cultivatedArea: string;
    longitude: number;
    latitude: number;
    slots: string[];
    sensors: string[];
}
