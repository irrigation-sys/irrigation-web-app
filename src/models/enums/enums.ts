export enum UserRole {
    SYSTEM_ADMIN = "system-admin",
    PlotLand_GROUP_ADMIN = "PlotLand-group-admin",
    PlotLand_ADMIN = "PlotLand-admin",
    STAGE_ADMIN = "stage-admin",
    GUARDIAN = "guardian",
    STUDENT = "student",
    TEACHER = "teacher",
}

export enum Privileges {
    // Admins
    GET_ADMINS = "getAdmins",
    DELETE_ADMIN = "deleteAdmin",
    CREATE_ADMIN = "createAdmin",
    UPDATE_ADMIN = "updateAdmin",
    // Students
    GET_STUDENTS = "getStudents",
    ADD_STUDENT = "addStudent",
    UPDATE_STUDENT = "updateStudent",
    DELETE_STUDENT = "deleteStudent",
    // Guardians
    GET_GUARDIANS = "getGuardians",
    CREATE_GUARDIAN = "createGuardian",
    UPDATE_GUARDIANS = "updateGuardians",
    DELETE_GUARDIANS = "deleteGuardians",
    // General
    UPDATE_USER_STATUS = "updateUserStatus",
    // PlotLand Groups
    FETCH_PlotLand_GROUPS = "fetchPlotLandGroups",
    CREATE_PlotLand_GROUP = "createPlotLandGroup",
    UPDATE_PlotLand_GROUP = "updatePlotLandGroup",
    DELETE_PlotLand_GROUP = "deletePlotLandGroup",
    // Semesters
    FETCH_SEMESTERS = "fetchSemesters",
    CREATE_SEMESTERS = "createSemester",
    UPDATE_SEMESTERS = "updateSemester",
    DELETE_SEMESTER = "deleteSemester",
    // Academic Year
    DELETE_ACADEMIC_YEAR = "deleteAcademicYear",
    UPDATE_ACADEMIC_YEAR = "updateAcademicYear",
    CREATE_ACADEMIC_YEAR = "createAcademicYear",
    FETCH_ACADEMIC_YEARS = "fetchAcademicYears",
    // Levels
    GET_LEVELS = "getLevels",
    GET_STAGE_LEVELS = "getStageLevels",
    DELETE_STAGE_LEVELS = "deleteStageLevels",
    UPDATE_STAGE_LEVELS = "updateStageLevels",
    CREATE_STAGE_LEVELS = "createStageLevels",
    // PlotLands
    FETCH_PlotLands = "fetchPlotLands",
    CREATE_PlotLand = "createPlotLand",
    UPDATE_PlotLand = "updatePlotLand",
    DELETE_PlotLand = "deletePlotLand",
    // Staff
    CREATE_STAFF = "createStaff",
    UPDATE_STAFF = "updateStaff",
    DELETE_STAFF = "deleteStaff",
    GET_STAFFS = "getStaffs",
    //Stages
    GET_PlotLand_STAGES = "getPlotLandstages",
    CREATE_PlotLand_STAGE = "createPlotLandstage",
    UPDATE_PlotLand_STAGE = "updatePlotLandstage",
    DELETE_PlotLand_STAGE = "deletePlotLandstage",
    // Courses
    CREATE_COURSE = "createCourse",
    UPDATE_COURSE = "updateCourse",
    FETCH_COURSE = "fetchCourse",
    DELETE_COURSE = "deleteCourse",
    // Assign Courses
    FETCH_ASSIGN_COURSES = "fetchAssignCourses",
    ASSIGN_COURSE = "assignCourse",
    UNASSIGN_COURSE = "unassignCourse",
    REASSIGN_COURSE = "reassignCourse",
    GET_TEACHER_ASSIGNED_COURSES = "getTeacherAssignedCourses",
    FETCH_ENROLLED_COURSES = "fetchEnrolledCourses",
    // Modules
    FETCH_MODULES = "fetchModules",
    CREATE_MODULE = "createModule",
    UPDATE_MODULE = "updateModule",
    DELETE_MODULE = "deleteModule",
    // Lessons
    FETCH_LESSON_PLANS = "fetchLessonPlans",
    OPEN_LESSON_PLAN = "openLessonPlan",
    PRINT_LESSON_PLAN = "printLessonPlan",
    CREATE_LESSON_PLAN = "createLessonPlan",
    UPDATE_LESSON_PLAN = "updateLessonPlan",
    DELETE_LESSON_PLAN = "deleteLessonPlan",
    // Module Material
    CREATE_MODULE_MATERIAL = "createModuleMaterial",
    UPDATE_MODULE_MATERIAL = "updateModuleMaterial",
    DELETE_MODULE_MATERIAL = "deleteModuleMaterial",
    CREATE_MODULE_MATERIAL_VIDEO = "createModuleMaterialVideo",
    UPDATE_MODULE_MATERIAL_VIDEO = "updateModuleMaterialVideo",
    DELETE_MODULE_MATERIAL_VIDEO = "deleteModuleMaterialVideo",
    // Discussion Room
    FETCH_DISCUSSION_ROOM = "fetchDiscussionRoom",
    CREATE_DISCUSSION_ROOM = "createDiscussionRoom",
    UPDATE_DISCUSSION_ROOM = "updateDiscussionRoom",
    DELETE_DISCUSSION_ROOM = "deleteDiscussionRoom",
    // Announcement
    FETCH_ANNOUNCEMENT = "fetchAnnouncement",
    CREATE_ANNOUNCEMENT = "createAnnouncement",
    UPDATE_ANNOUNCEMENT = "updateAnnouncement",
    DELETE_ANNOUNCEMENT = "deleteAnnouncement",
    // VCRs
    FETCH_VCRS = "fetchVcrs",
    CREATE_VCR = "createVcr",
    UPDATE_VCR = "updateVcr",
    DELETE_VCR = "deleteVcr",
    // VCR Records
    CREATE_VCR_RECORD = "createVcrRecord",
    UPDATE_VCR_RECORD = "updateVcrRecord",
    DELETE_VCR_RECORD = "deleteVcrRecord",
    //Behaviour Groups
    GET_BEHAVIOUR_GROUPS = "getBehaviourGroups",
    CREATE_BEHAVIOUR_GROUPS = "createBehaviourGroups",
    UPDATE_BEHAVIOUR_GROUPS = "updateBehaviourGroups",
    DELETE_BEHAVIOUR_GROUPS = "deleteBehaviourGroups",
    // Question bank groups
    GET_QUESTION_BANK_GROUPS = "getBehaviourGroups",
    CREATE_QUESTION_BANK_GROUPS = "createBehaviourGroups",
    UPDATE_QUESTION_BANK_GROUPS = "updateBehaviourGroups",
    DELETE_QUESTION_BANK_GROUPS = "deleteBehaviourGroups",
    //Actions
    GET_ACTIONS = "getActions",
    CREATE_ACTIONS = "createActions",
    UPDATE_ACTIONS = "updateActions",
    DELETE_ACTIONS = "deleteActions",

    GET_BEHAVIOURS = "getBehaviours",
    CREATE_BEHAVIOURS = "createBehaviours",
    UPDATE_BEHAVIOURS = "updateBehaviours",
    DELETE_BEHAVIOURS = "deleteBehaviours",

    GET_QUESTION_BANKS = "getQuestionBanks",
    CREATE_QUESTION_BANKS = "createQuestionBanks",
    UPDATE_QUESTION_BANKS = "updateQuestionBanks",
    DELETE_QUESTION_BANKS = "deleteQuestionBanks",

    CREATE_QUESTION = "createQuestion",
    UPDATE_QUESTION = "updateQuestion",
    DELETE_QUESTION = "deleteQuestion",
    FETCH_QUESTIONS = "fetchQuestions",

    CREATE_ACTIVITY_GROUP = "createActivityGroup",
    UPDATE_ACTIVITY_GROUP = "updateActivityGroup",
    DELETE_ACTIVITY_GROUP = "deleteActivityGroup",
    FETCH_ACTIVITY_GROUP = "fetchActivityGroups",
}

export enum YearSemesterStatus {
    NOT_STARTED = "not_started",
    CURRENT = "current",
    ENDED = "ended",
}

export enum Gender {
    MALE = "male",
    FEMALE = "female",
}

export enum PersonType {
    STUDENT = "student",
    GUARDIAN = "guardian",
    SECONDARY_GUARDIAN = "secondary-guardian",
    PARENT = "parent",
    SYSTEM_ADMIN = "system-admin",
    PlotLand_GROUP_ADMIN = "PlotLand-group-admin",
    PlotLand_ADMIN = "PlotLand-admin",
    STAGE_ADMIN = "stage-admin",
    STAFF = "staff",
}

export enum RELIGION {
    ISLAM = "Islam",
    CHRISTIANITY = "Christianity",
    JUDAISM = "Judaism",
    OTHER = "Other",
}

export enum NationalIdType {
    NATIONALID = "national_id",
    PASSPORT = "passport",
}

export enum RelationType {
    FATHER = "father",
    MOTHER = "mother",
    RELATIVE = "relative",
    CHILD = "child",
    ADMIN = "admin",
}

export enum GuardianRelationType {
    FATHER = "father",
    MOTHER = "mother",
    RELATIVE = "relative",
}

export enum COURSE_CATEGROY {
    ENGLISH = "English",
    ARABIC = "Arabic",
    MATH = "Math",
    SCIENCE = "Science",
    MATHEMATICS = "Mathematics",
    ENGLISH_READING = "English - Reading",
    ENGLISH_WRITING = "English Writing",
    HISTORY = "History",
    GEOGRAPHY = "Geography",
    DESIGN_TECHNOLOGY = "Design and Technology",
    FRENCH = "French",
    AFL = "Arabic as a Foreign Language (AFL)",
    MUSIC = "Music",
    SOCIAL_STUDIES = "Social studies",
    PE = "Physical Education (PE)",
    PSHE = "Personal Social Health Education (PSHE)",
    IGCSE = "IGCSE",
    ARABIC_FIRST_LANUAGE = "Arabic First Lanuage",
    ARABIC_FORIGN_LANUAGE = "Arabic Forign Lanuage",
    ENGLISH_LANUAGE_A = "English Lanuage A",
    ENGLISH_LITERATURE = "English Literature",
    ART_DESIGN = "Art and design",
    DRAMA = "Drama",
    FIRST_LANGUAGE_ENGLISH = "First Language English",
    ENGLISH_SECOND_LANGUAGE = "English as a second Language",
    GEOGRAOHY = "Geograohy",
    ICT_COMPUTING = "ICT - Computing",
    BIOLOGY = "Biology",
    Chemistry = "Chemistry",
    PHYSICS = "physics",
    BUSINIS_STUDIES = "Businis studies",
    ECONOMICS = "Economics",
    PSYCHOLOGY = "Psychology",
    COMPUTER_SCIENCE = "Computer science",
    SOCIOLOGY = "Sociology",
}

export const SYSTEM_NAME = "Irrigation";

export enum Mail {
    ALL = "ALL",
    TRASH = "TRASH",
    DRAFTS = "DRAFTS",
}

export enum StaffType {
    TEACHER = "teacher",
    SUPERVISOR = "supervisor",
    MANAGER = "manager",
}

export enum NOTIFICATION_TYPES {
    POST = "POST",
    COMMENT = "COMMENT",
}

export enum TAXONOMY {
    NONE = "None",
    UNDERSTANDING = "Understanding",
    APPLYING = "Applying",
    ANALYZING = "Analyzing",
}

export enum MaterialTypes {
    VIDEO = "video",
    YOUTUBE_LINK = "youtubeLink",
    COURSE_MATERIAL = "courseMaterial",
    EXTERNAL_URL = "externalUrl",
}

export enum SharingLevels {
    PUBLIC = "public",
}

export enum DiscussionOptions {
    THREADED_REPLIES = "THREADED_REPLIES",
    ALLOW_STUDENTS_TO_CREATE_POSTS = "ALLOW_STUDENTS_TO_CREATE_POSTS",
    ALLOW_MULTIPLE_POSTS = "ALLOW_MULTIPLE_POSTS",
}

export enum Curriculum {
    BRITISH = "BRITISH",
    AMERICAN = "AMERICAN",
    NATIONAL = "NATIONAL",
    IB = "IB",
    SEMI_INTERNATIONAL = "SEMI_INTERNATIONAL",
}

export enum MeetingVendor {
    MICROSOFT_TEAMS = "MICROSOFT_TEAMS",
    ZOOM = "ZOOM",
    GOOGLE_MEET = "GOOGLE_MEET",
}
