/* eslint-disable @typescript-eslint/ban-ts-comment */
import { DropDownOption } from "../inputs/DropDownOption";

export function getEnumAsOptions(t: any, eNum: any) {
    const options: DropDownOption[] = [];
    const keys = Object.keys(eNum);
    for (const eName of keys) {
        options.push({
            value: eName,
            //@ts-ignore
            displayAr: t(eNum[eName]),
            //@ts-ignore
            displayEn: eNum[eName],
        });
    }

    return options;
}
