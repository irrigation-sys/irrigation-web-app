export enum OfficalGuardian {
    NotSet = "NotSet",
    Father = "Father",
    Mother = "Mother",
}
