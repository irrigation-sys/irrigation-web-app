export interface DropDownOption {
    value?: string;
    displayEn?: string;
    displayAr?: string;
    //you can use this data to store anything inside the option
    data?: any;
}
