import { Box, Flex } from "@chakra-ui/react";
import NavBar from "../components/navigation/Navbar";
import Sidebar from "../components/navigation/side-bar/SideBar";
import { dashboardContext } from "../context/DashboardContext";
import useDashBoardHook from "../hooks/useDashBoardHook";

export default function PageBase({ children }: any) {
    const dashBoardState = useDashBoardHook();
    return (
        <dashboardContext.Provider value={dashBoardState}>
            <Flex flexDir="row">
                <Box height="100vh">
                    <Sidebar />
                </Box>
                <Box as="div" className={`px-10 ${dashBoardState.isSideBarOpen ? "w-4/5" : "w-full"}`}>
                    <NavBar />
                    <Box>
                        <Box>{children}</Box>
                    </Box>
                </Box>
            </Flex>
        </dashboardContext.Provider>
    );
}
