import { Box, Button, FormControl, FormLabel, HStack, Input, useDisclosure } from "@chakra-ui/react";
import { useEffect, useState } from "react";
import { useForm } from "react-hook-form";
import { useTranslation } from "react-i18next";
import { FaCheckCircle, FaExclamationCircle } from "react-icons/fa";
import { Link, useHistory } from "react-router-dom";
import { ErrorMessage } from "../../components/FormHelpers";
import CancelModal from "../../components/modals/CancelModal";
import NavArrow from "../../components/navigation/NavArrow";
import { PlotLandModel } from "../../models/PlotLandModel";
import { useEditPlotLand } from "../../service/PlotLandservice";

type FormValues = {
    name: string;
    code: string;
    waterAmount: number;
    agriculturalCrop: string;
    cultivatedArea: string;
    longitude: number;
    latitude: number;
};

function EditPlotLand() {
    const history = useHistory();
    const { state } = history.location;
    const editPlotLand = (state as any).editPlotLand as PlotLandModel;

    const [errorMessage, setErrorMessage] = useState("");

    const { isOpen, onOpen, onClose } = useDisclosure();

    const {
        control,
        register,
        handleSubmit,
        formState: { errors },
        setValue,
        watch,
    } = useForm<FormValues>();

    const {
        t,
        i18n: { language },
    } = useTranslation();

    const { data, isLoading, error, mutate } = useEditPlotLand();

    const submitPlotLand = (plotLand: FormValues) => {
        mutate({ ...plotLand, id: editPlotLand.id });
    };

    useEffect(() => {
        if (error && (error as any).response.data.message) {
            let message = (error as any).response.data.message;
            if (message.indexOf("duplicate") > -1) {
                message = "The code you’ve entered is taken, please enter another code.";
            }
            setErrorMessage(message);
        }
    }, [error]);

    const cancelOperationHandler = () => {
        history.push("PlotLands");
    };

    useEffect(() => {
        setValue("name", editPlotLand.name);
        setValue("agriculturalCrop", editPlotLand.agriculturalCrop);
        setValue("code", editPlotLand.code);
        setValue("cultivatedArea", editPlotLand.cultivatedArea);
        setValue("latitude", editPlotLand.latitude);
        setValue("longitude", editPlotLand.longitude);
        setValue("waterAmount", editPlotLand.waterAmount);
    }, []);

    return (
        <>
            <CancelModal
                header={t("Cancel Operation")}
                body={t("Are you sure you want to cancel this operation?")}
                noText={t("No Continue")}
                yesText={t("Yes Cancel the Operation")}
                isOpen={isOpen}
                onClose={onClose}
                onSubmit={cancelOperationHandler}
            />
            <div className="h-screen text-gray-500">
                <main className="pt-4 mx-auto">
                    <div className="text-base text-black space-s-3">
                        <Link to="/home" className="font-semibold">
                            {t("Home")}
                        </Link>
                        <NavArrow />
                        <Link to="/PlotLands" className="font-semibold">
                            {t("Plot Lands Management")}
                        </Link>
                        <NavArrow />
                        <label className="font-semibold text-gray-500">
                            {t("Edit")} - {editPlotLand.name}
                        </label>
                    </div>

                    <div className="pt-8 text-lg font-semibold text-black">
                        {t("Edit")} - {editPlotLand.name}
                    </div>

                    <div className="pt-3 text-sm text-gray-400">{t("Enter the below data, be careful.")}</div>

                    <div className="mt-6">
                        {errorMessage && (
                            <div
                                className="flex items-center px-4 py-3 mb-6 font-semibold text-red-500 bg-red-100 rounded text-md"
                                role="alert">
                                <FaExclamationCircle className="mr-5" />
                                <span>{errorMessage}</span>
                            </div>
                        )}

                        {data && (
                            <div
                                className="flex items-center px-4 py-3 mb-6 font-semibold text-green-500 bg-green-100 rounded text-md"
                                role="alert">
                                <FaCheckCircle className="mr-5" />
                                <span>
                                    {data.name} {t("is updated successfully")}
                                </span>
                            </div>
                        )}

                        <form onSubmit={handleSubmit(submitPlotLand)}>
                            <Box p="40px" color="black" mt="4" bg="brandGreyForm" rounded="md" shadow="md">
                                <HStack spacing="24px" pt={"32px"}>
                                    <FormControl id="name">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Name") + " *"}
                                            {errors.name && <ErrorMessage message={errors.name.message} />}
                                        </FormLabel>
                                        <Input
                                            autoFocus
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="name"
                                            fontSize="12"
                                            {...register("name", { required: `${t("Name is required")}` })}
                                            isInvalid={errors?.name ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>
                                    <FormControl id="Code">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Code") + " *"}
                                            {errors.code && <ErrorMessage message={errors.code.message} />}
                                        </FormLabel>
                                        <Input
                                            type={"number"}
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="points"
                                            fontSize="12"
                                            {...register("code", { required: `${t("Code is required")}` })}
                                            isInvalid={errors?.code ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>
                                </HStack>

                                <HStack spacing="24px" pt={"32px"}>
                                    <FormControl id="agriculturalCrop">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Agricultural Crop") + " *"}
                                            {errors.agriculturalCrop && (
                                                <ErrorMessage message={errors.agriculturalCrop.message} />
                                            )}
                                        </FormLabel>
                                        <Input
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="agriculturalCrop"
                                            fontSize="12"
                                            {...register("agriculturalCrop", {
                                                required: `${t("Agricultural Crop is required")}`,
                                            })}
                                            isInvalid={errors?.agriculturalCrop ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>

                                    <FormControl id="cultivatedArea">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Cultivated Area") + " *"}
                                            {errors.cultivatedArea && (
                                                <ErrorMessage message={errors.cultivatedArea.message} />
                                            )}
                                        </FormLabel>
                                        <Input
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="cultivatedArea"
                                            fontSize="12"
                                            {...register("cultivatedArea", {
                                                required: `${t("Cultivated Area is required")}`,
                                            })}
                                            isInvalid={errors?.cultivatedArea ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>
                                </HStack>

                                <HStack spacing="24px" pt={"32px"}>
                                    <FormControl id="waterAmount">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Water Amount") + " *"}
                                            {errors.waterAmount && (
                                                <ErrorMessage message={errors.waterAmount.message} />
                                            )}
                                        </FormLabel>
                                        <Input
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="waterAmount"
                                            fontSize="12"
                                            {...register("waterAmount", {
                                                required: `${t("Water Amount is required")}`,
                                            })}
                                            isInvalid={errors?.waterAmount ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>

                                    <FormControl />
                                </HStack>

                                <HStack spacing="24px" pt={"32px"}>
                                    <FormControl id="longitude">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Longitude") + " *"}
                                            {errors.longitude && <ErrorMessage message={errors.longitude.message} />}
                                        </FormLabel>
                                        <Input
                                            type={"number"}
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="longitude"
                                            fontSize="12"
                                            {...register("longitude", { required: `${t("Longitude is required")}` })}
                                            isInvalid={errors?.longitude ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>
                                    <FormControl id="latitude">
                                        <FormLabel fontWeight="600" fontSize="14">
                                            {t("Latitude") + " *"}
                                            {errors.latitude && <ErrorMessage message={errors.latitude.message} />}
                                        </FormLabel>
                                        <Input
                                            type={"number"}
                                            backgroundColor="#ffffff"
                                            size="lg"
                                            id="latitude"
                                            fontSize="12"
                                            {...register("latitude", { required: `${t("Latitude is required")}` })}
                                            isInvalid={errors?.latitude ? true : false}
                                            errorBorderColor="red.300"
                                        />
                                    </FormControl>
                                </HStack>

                                <HStack mt={"26px"} pb={"32px"}>
                                    <FormControl>
                                        <Button
                                            type="submit"
                                            w="100%"
                                            isLoading={isLoading}
                                            variant="solid"
                                            className="skv-btn-primary"
                                            _hover={{ bg: "#5E8CF1" }}>
                                            {t("Save Plot Land")}
                                        </Button>
                                    </FormControl>

                                    <FormControl>
                                        <Button
                                            variant="ghost"
                                            size="lg"
                                            fontSize="14"
                                            fontWeight="600"
                                            onClick={onOpen}
                                            className="ml-0 mr-0">
                                            {t("Cancel")}
                                        </Button>
                                    </FormControl>
                                </HStack>
                            </Box>
                        </form>
                    </div>
                </main>
            </div>
        </>
    );
}

export default EditPlotLand;
