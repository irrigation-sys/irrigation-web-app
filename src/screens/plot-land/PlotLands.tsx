import { useMemo } from "react";
import { useTranslation } from "react-i18next";
import { FaEdit } from "react-icons/fa";
import { useHistory } from "react-router";
import { Link } from "react-router-dom";
import { ReactComponent as PlotLandGrayIcon } from "../../assets/school-gray.svg";
import NavArrow from "../../components/navigation/NavArrow";
import { PlaceholderTable } from "../../components/table/placeholder-message-table";
import Table from "../../components/table/Table";
import { useGetPlotLands } from "../../service/PlotLandservice";

function PlotLands() {
    const {
        t,
        i18n: { language },
    } = useTranslation();
    const { data: plotLands, isLoading } = useGetPlotLands();
    const history = useHistory();

    const handleEdit = (original: any) => {
        history.push("edit-PlotLand", { editPlotLand: original });
    };

    const columns = useMemo(
        () => [
            {
                Header: "#",
                Cell: (row: any) => {
                    return <div className="text-black">{Number(row.row.id) + 1}</div>;
                },
            },
            {
                Header: t("Code"),
                accessor: "code",
            },
            {
                Header: t("Name"),
                accessor: "name",
            },
            {
                Header: t("Water Amount"),
                accessor: "waterAmount",
            },
            {
                Header: t("Agricultural Crop"),
                accessor: "agriculturalCrop",
            },
            {
                Header: t("Cultivated Area"),
                accessor: "cultivatedArea",
            },
            {
                Header: t("Actions"),
                Cell: row => (
                    <div className="flex items-baseline space-x-2">
                        <button
                            type="button"
                            className="px-4 py-2 text-xs rounded-md me-2 btn-primary text-gray-50"
                            onClick={() => handleEdit(row.row.original)}>
                            <FaEdit />
                            <span className="ps-2">{t("Edit")}</span>
                        </button>
                    </div>
                ),
            },
        ],
        [plotLands, language]
    );

    return (
        <>
            <div className="min-h-screen text-gray-500">
                <main className="pt-4 mx-auto">
                    <div className="text-base text-black space-s-3">
                        <Link to="/home" className="font-semibold">
                            {t("Home")}
                        </Link>
                        <NavArrow />
                        <label className="font-semibold text-gray-500"> {t("Plot Lands Management")}</label>
                    </div>
                    <div className="mt-6">
                        <Table
                            columns={columns}
                            data={plotLands ?? []}
                            searchPlaceholder={t("What are you searching for?")}
                            createText={t("Add New Plot Land")}
                            enableCreate={true}
                            handleCreate={() => history.push("create-PlotLand")}
                            EmptyDataMessage={
                                <PlaceholderTable
                                    Icon={<PlotLandGrayIcon />}
                                    heading={t("No Plot Lands Found")}
                                    message={t("You Can Easily Create New Plot Land Now.")}
                                />
                            }
                            isLoading={isLoading}
                        />
                    </div>
                </main>
            </div>
        </>
    );
}

export default PlotLands;
