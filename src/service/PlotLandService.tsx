import { useMutation, useQuery } from "react-query";
import axiosApiInstance from "../helpers/AxiosClient";
import { PlotLandModel } from "../models/PlotLandModel";

const API_URL = import.meta.env.VITE_PlotLand!;

export function useGetPlotLands() {
    const res = useQuery(
        "PlotLands",
        (): Promise<PlotLandModel[]> =>
            axiosApiInstance.get(API_URL).then(response => {
                return response.data._embedded.plotLandModelList;
            })
    );
    return res;
}

export function useCreatePlotLand() {
    return useMutation((plotLand: any) =>
        axiosApiInstance.post(API_URL, { ...plotLand }).then(response => {
            return response.data;
        })
    );
}

export function useEditPlotLand() {
    return useMutation((plotLand: any) =>
        axiosApiInstance.put(API_URL + plotLand.code, { ...plotLand }).then(response => {
            return response.data;
        })
    );
}
