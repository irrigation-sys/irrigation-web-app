import { ChakraProvider, extendTheme } from "@chakra-ui/react";
import { useTranslation } from "react-i18next";

function ChakraRTLProvider({ children }: any) {
  const {
    i18n: { language },
  } = useTranslation();
  const direction = language === "ar" ? "rtl" : "ltr";

  const customTheme = extendTheme({
    colors: {
      transparent: "transparent",
      black: "#000",
      white: "#fff",
      brandBlue: "#4e73f8",
      brandLightBlue: "#4e73f814",
      brandGrey: "#F1F2F3",
      brandSilver: "#757D8A",
      brandGreyForm: "#f7f7f7",
      brandLightGrey: "#F8F8F8",
      brandGreen: "#07A287",
      brandHollowRed: "#F9E8E8",
      brandRed: "#F43C3C",
      brandLightRed: "#F06767",
      brandHollowGreen: "#07A28714",
      brandLightBrand: "#dae0f7",
      brandDarkGreen: "#08B295",
      green: {
        50: "#e6fcf5",
        100: "#07A287",
        200: "#85d17e",
        300: "#5ab051",
        400: "#3a9f2f",
        500: "rgba(7, 162, 135, 0.08)",
        600: "#2a8d1f",
        700: "#2a8d1f",
        800: "#2a8d1f",
        900: "#2a8d1f",
      },
      red: {
        100: "#F06767",
        500: "rgba(244, 60, 60, 0.08)",
      },
      mainBrandBlue: {
        100: "#4E73F8",
        500: "rgba(78, 115, 248, 0.08)",
      },
    },
    // I'm just adding one more fontSize than the default ones
    fontSizes: {
      xxs: "0.625rem",
    },
    // I'm creating a new space tokens since the default is represented with numbers
    space: {
      xs: "0.25rem",
      sm: "0.5rem",
      md: "1rem",
      lg: "1.5rem",
      xl: "2rem",
      xxl: "3rem",
    },
    direction,
  });

  return <ChakraProvider theme={customTheme}>{children}</ChakraProvider>;
}

export default ChakraRTLProvider;
