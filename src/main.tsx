import React from "react";
import ReactDOM from "react-dom";
import { QueryClient, QueryClientProvider } from "react-query";
import { BrowserRouter } from "react-router-dom";
import App from "./App";
import ChakraRTLProvider from "./CustomTheme";
import "./i18n/config";
import "./index.css";
import "regenerator-runtime/runtime";

const queryClient = new QueryClient();

ReactDOM.render(
    <React.StrictMode>
        <QueryClientProvider client={queryClient}>
            <BrowserRouter>
                <ChakraRTLProvider>
                    <App />
                </ChakraRTLProvider>
            </BrowserRouter>
            {/* <ReactQueryDevtools initialIsOpen /> */}
        </QueryClientProvider>
    </React.StrictMode>,
    document.getElementById("root")
);
