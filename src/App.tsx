import { Box } from "@chakra-ui/react";
import { useEffect } from "react";
import { useTranslation } from "react-i18next";
import "./App.css";
import MainRoutes from "./MainRoutes";

function App() {
    const {
        i18n: { language },
    } = useTranslation();
    const direction = language === "ar" ? "rtl" : "ltr";

    useEffect(() => {
        document.documentElement.dir = direction;
    }, [language]);
    return (
        <Box className="App" dir={direction} lang={language}>
            <MainRoutes />
        </Box>
    );
}

export default App;
