/// <reference types="vite/client" />
interface ImportMetaEnv {
    readonly VITE_APP_TITLE: string;
    readonly VITE_PlotLand: string;
    readonly VITE_PlotLandPrediction: string;
}

interface ImportMeta {
    readonly env: ImportMetaEnv;
}
